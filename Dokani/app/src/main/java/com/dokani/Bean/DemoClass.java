package com.dokani.Bean;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by brst-pc20 on 1/25/17.
 */

public class DemoClass extends RealmObject {


    private String categoryData,bannerData,cartProductName,cartProductUrl,cartProductPrice,cartProductId,cartProductRating,cartProductQuantity,cartProductAvailabilityQty;

    private String newArrivalData;

    private String wishProductName,wishProductUrl, wishProductPrice, wishProductId, wishProductRating,wishProductDesc,wishProductAvailabilityQty;

    @PrimaryKey
    private long id;

    public String getCategoryData() {
        return categoryData;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCartProductQuantity() {
        return cartProductQuantity;
    }

    public String getCartProductAvailabilityQty() {
        return cartProductAvailabilityQty;
    }

    public void setCartProductAvailabilityQty(String cartProductAvailabilityQty) {
        this.cartProductAvailabilityQty = cartProductAvailabilityQty;
    }

    public String getWishProductAvailabilityQty() {
        return wishProductAvailabilityQty;
    }

    public void setWishProductAvailabilityQty(String wishProductAvailabilityQty) {
        this.wishProductAvailabilityQty = wishProductAvailabilityQty;
    }

    public void setCartProductQuantity(String cartProductQuantity) {
        this.cartProductQuantity = cartProductQuantity;
    }

    public String getWishProductDesc() {
        return wishProductDesc;
    }

    public void setWishProductDesc(String wishProductDesc) {
        this.wishProductDesc = wishProductDesc;
    }

    public String getNewArrivalData() {
        return newArrivalData;
    }

    public void setNewArrivalData(String newArrivalData) {
        this.newArrivalData = newArrivalData;
    }

    public String getCartProductId() {
        return cartProductId;
    }

    public void setCartProductId(String cartProductId) {
        this.cartProductId = cartProductId;
    }

    public String getCartProductRating() {
        return cartProductRating;
    }

    public void setCartProductRating(String cartProductRating) {
        this.cartProductRating = cartProductRating;
    }

    public String getWishProductName() {
        return wishProductName;
    }

    public void setWishProductName(String wishProductName) {
        this.wishProductName = wishProductName;
    }

    public String getWishProductUrl() {
        return wishProductUrl;
    }

    public void setWishProductUrl(String wishProductUrl) {
        this.wishProductUrl = wishProductUrl;
    }

    public String getWishProductPrice() {
        return wishProductPrice;
    }

    public void setWishProductPrice(String wishProductPrice) {
        this.wishProductPrice = wishProductPrice;
    }

    public String getWishProductId() {
        return wishProductId;
    }

    public void setWishProductId(String wishProductId) {
        this.wishProductId = wishProductId;
    }

    public String getWishProductRating() {
        return wishProductRating;
    }

    public void setWishProductRating(String wishProductRating) {
        this.wishProductRating = wishProductRating;
    }

    public String getBannerData() {

        return bannerData;
    }

    public String getCartProductName() {
        return cartProductName;
    }

    public void setCartProductName(String cartProductName) {
        this.cartProductName = cartProductName;
    }

    public String getCartProductUrl() {
        return cartProductUrl;
    }

    public void setCartProductUrl(String cartProductUrl) {
        this.cartProductUrl = cartProductUrl;
    }

    public String getCartProductPrice() {
        return cartProductPrice;
    }

    public void setCartProductPrice(String cartProductPrice) {
        this.cartProductPrice = cartProductPrice;
    }

    public void setBannerData(String bannerData) {
        this.bannerData = bannerData;
    }

    public void setCategoryData(String categoryData) {
        this.categoryData = categoryData;
    }
}
