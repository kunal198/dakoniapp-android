package com.dokani.Bean;

/**
 * Created by brst-pc20 on 1/9/17.
 */

public class Cartbean {

    private int setQuantity;
    private int getQuantity;

    public int getSetQuantity() {
        return setQuantity;
    }

    public void setSetQuantity(int setQuantity) {
        this.setQuantity = setQuantity;
    }

    public int getGetQuantity() {
        return getQuantity;
    }

    public void setGetQuantity(int getQuantity) {
        this.getQuantity = getQuantity;
    }
}
