package com.dokani.Bean;

import java.util.ArrayList;

/**
 * Created by brst-pc20 on 1/11/17.
 */

public class HomePageBean {
   private String imageUrl,productName,productUrl,productPrice,category_name,category_image_url;
    private String productAvailableQty,productId,productRating,productSpecialPrice,productSmallDesc;

    private ArrayList<String > main_list=new ArrayList<>();

    public String getImageUrl() {
        return imageUrl;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductRating() {
        return productRating;
    }

    public void setProductRating(String productRating) {
        this.productRating = productRating;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getProductSpecialPrice() {
        return productSpecialPrice;
    }

    public String getProductAvailableQty() {
        return productAvailableQty;
    }

    public void setProductAvailableQty(String productAvailableQty) {
        this.productAvailableQty = productAvailableQty;
    }

    public void setProductSpecialPrice(String productSpecialPrice) {
        this.productSpecialPrice = productSpecialPrice;
    }

    public String getProductSmallDesc() {
        return productSmallDesc;
    }

    public void setProductSmallDesc(String productSmallDesc) {
        this.productSmallDesc = productSmallDesc;
    }

    public ArrayList<String> getMain_list() {
        return main_list;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getCategory_image_url() {
        return category_image_url;
    }

    public void setCategory_image_url(String category_image_url) {
        this.category_image_url = category_image_url;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public void setMain_list(ArrayList<String> main_list) {
        this.main_list = main_list;
    }
}
