package com.dokani.CommonUtils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.dokani.Activity.FragmentActivity;
import com.dokani.Activity.HomePage;
import com.dokani.Async_Thread.Super_AsyncTask;
import com.dokani.Bean.DemoClass;
import com.dokani.Fragments.LoginFragment;
import com.dokani.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by brst-pc20 on 1/3/17.
 */

public class Constant {


    ProgressDialog progress;
    public static String BASE_URL = "http://dokani.net/new/index.php/dokaniapi/index/";
    public static String BANNER_API = BASE_URL + "gethomebanner";
    public static String CATEGORY_API = BASE_URL + "getcategories";
    public static String NEW_ARRIVAL_PRODUCT_API = BASE_URL + "getfeaturedproduct";
    public static String GET_PRODUCT_FOR_CATEGORY_API = BASE_URL + "productforcategories";
    public static String LOGIN_API = BASE_URL + "loginwithemail";
    public static String REGISTER_WITH_EMAIL = BASE_URL + "registerwithemail";
    public static String SORT_PRODUCTS = BASE_URL + "sortproducts";
    public static String PRODUCT_DETAILS = BASE_URL + "productdetails";
    public static String RECENT_PRODUCTS = BASE_URL + "getrecentproducts";
    public static String POPULAR_PRODUCTS = BASE_URL + "getpopularproducts";
    public static String FILTERABLE_LIST = BASE_URL + "filterlist";
    public static String PRODUCTS_BY_FILTER = BASE_URL + "getproductsbyfilter";
    public static String SEARCH_PRODUCTS = BASE_URL + "searchproducts";
    public static String FORGOT_PASSWORD = BASE_URL + "forgotpassword";
    public static String SOCIAL_LOGIN = BASE_URL + "social";
    public static String EDIT_PROFILE = BASE_URL + "editprofile";
    public static String SHIPPING_CHARGES = BASE_URL + "getshippingcharges";
    public static String RATING_API = BASE_URL + "rating";
    public static String CHECK_QTY = BASE_URL + "checkqty";
    public static String STRIPE = BASE_URL + "stripe";
    // public static String GET_COUNTRIES="http://api.geonames.org/countryInfoJSON?username=brst";
    public static String GET_CITIES = "http://api.geonames.org/childrenJSON?geonameId=";
    public static String GEONAME_USERNAME = "&username=brst";

    public static String PAYMENT_API = BASE_URL + "createorder";
  //  http://beta.brstdev.com/dokaninet/index.php/dokaniapi/index/stripe

    public static ArrayList<String> category_list = new ArrayList<>();


    private static Context context;

    public Constant(Context context) {
        this.context = context;
    }

    public static boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI")) {
                if (ni.isConnected()) {
                    haveConnectedWifi = true;
                    Log.v("WIFI CONNECTION ", "AVAILABLE");
                } else {
                    Log.v("WIFI CONNECTION ", "NOT AVAILABLE");
                }
            }
            if (ni.getTypeName().equalsIgnoreCase("MOBILE")) {
                if (ni.isConnected()) {
                    haveConnectedMobile = true;

                } else {

                }
            }
        }
        if (!haveConnectedWifi && !haveConnectedMobile) {
            showAlert2(context, context.getResources().getString(R.string.alert), "Internet Connection Required");
        }
        return haveConnectedWifi || haveConnectedMobile;
    }


    public static ArrayList<Integer> categoryImageData() {
        ArrayList<Integer> category_list = new ArrayList<>();

        category_list.add(R.mipmap.ic_desktop);
        category_list.add(R.mipmap.ic_mobile);
        category_list.add(R.mipmap.ic_tablets);
        category_list.add(R.mipmap.ic_laptops);
        category_list.add(R.mipmap.ic_cpu);
        category_list.add(R.mipmap.ic_headphones);
        category_list.add(R.mipmap.ic_television);
        category_list.add(R.mipmap.ic_appliances);
        return category_list;
    }

    public static ArrayList<Integer> newArrivalImagesData() {
        ArrayList<Integer> category_list = new ArrayList<>();

        category_list.add(R.mipmap.newarrival1);
        category_list.add(R.mipmap.newarrival2);
        category_list.add(R.mipmap.newarrival3);
        category_list.add(R.mipmap.newarrival1);
        category_list.add(R.mipmap.newarrival2);
        category_list.add(R.mipmap.newarrival3);
        category_list.add(R.mipmap.newarrival1);
        category_list.add(R.mipmap.newarrival2);
        category_list.add(R.mipmap.newarrival3);
        category_list.add(R.mipmap.newarrival1);
        category_list.add(R.mipmap.newarrival2);
        category_list.add(R.mipmap.newarrival3);

        return category_list;
    }

    public static ArrayList<String> categoryNameData() {


        category_list.add("Desktop");
        category_list.add("Mobile Phones");
        category_list.add("Tablets");
        category_list.add("Laptops");
        category_list.add("C.P.U's");
        category_list.add("Headphones");
        category_list.add("Televisions");
        category_list.add("Appliances");

        return category_list;
    }

    public static SwipeMenuCreator swipeCreator() {
        SwipeMenuCreator swipeMenuCreator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem openItem = new SwipeMenuItem(context);
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                openItem.setWidth(250);
                // set item title
                openItem.setTitle("Delete");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);


            }

        };
        return swipeMenuCreator;
    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static void initiatePopupWindow(String msg, Context con) {
        try {
// We need to get the instance of the LayoutInflater
            LayoutInflater inflater = (LayoutInflater) con
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.popup_screen, null, true);

            TextView alertMessageTV = (TextView) layout.findViewById(R.id.alertMessageTV);
            //  int height= (int) (screenHeight/2.5);
            final PopupWindow pwindo = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
            pwindo.showAtLocation(layout, Gravity.CENTER, 0, 0);

            if (!msg.isEmpty())
                alertMessageTV.setText(msg);
            TextView okTV = (TextView) layout.findViewById(R.id.okTV);
            okTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pwindo.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void execute(Super_AsyncTask asyncTask) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            asyncTask.execute();
        }

    }

    public static void copyDataToRealm(Realm realm, RealmObject demoClass) {
        realm.beginTransaction();
        realm.copyToRealm(demoClass);
        realm.commitTransaction();
    }


    public static boolean validateFields(String data) {
        if (data.isEmpty()) {
            return false;
        }
        return true;
    }

    public static boolean validatelength(String data) {
        if (data.length() < 8) {
            return false;
        }
        return true;
    }

    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean matchPassword(String pass1, String pass2) {
        if (!pass1.equals(pass2)) {
            return true;
        }
        return false;
    }

    public static void showAlert(final Fragment context, String title, String msg) {
        new AlertDialog.Builder(context.getActivity())
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

                        FragmentActivity.fragActivitytitleTV.setText(context.getResources().getString(R.string.login));
                        LoginFragment loginFragment = new LoginFragment();
                        context.getActivity().getSupportFragmentManager().beginTransaction()
                                .add(R.id.fragmentsContainer, loginFragment)
                                .commit();

                    }
                })
                /*.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })*/
                .setIcon(R.mipmap.ic_alert)
                .show();
    }


    public static String upload(HashMap map,InputStream profile_pic_data) {
        Log.d("mappppppppp", "---" + profile_pic_data);

        Log.d("log_tag", "--" + map.get("firstname").toString());
        HttpPost httppost = new HttpPost(Constant.EDIT_PROFILE);
        // HttpParams httpParameters = new BasicHttpParams();
        HttpClient httpclient = new DefaultHttpClient();
        try {
            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);


            if (profile_pic_data!=null)
            {
                reqEntity.addPart("profile", new InputStreamBody(profile_pic_data, "dp.jpg"));
                reqEntity.addPart("mimeType", new StringBody("image/jpeg"));
            }


            reqEntity.addPart("firstname", new StringBody(map.get("firstname").toString()));
            reqEntity.addPart("email", new StringBody(map.get("email").toString()));
            reqEntity.addPart("customer_id", new StringBody(map.get("customer_id").toString()));

            reqEntity.addPart("mobile", new StringBody(map.get("mobile").toString()));
            reqEntity.addPart("gender", new StringBody(map.get("gender").toString()));
            reqEntity.addPart("age", new StringBody(map.get("age").toString()));

            reqEntity.addPart("street", new StringBody(map.get("street").toString()));
            reqEntity.addPart("region", new StringBody(map.get("region").toString()));
            reqEntity.addPart("city", new StringBody(map.get("city").toString()));
            reqEntity.addPart("postcode", new StringBody(map.get("postcode").toString()));
            httppost.setEntity(reqEntity);


            HttpResponse response = httpclient.execute(httppost);
            String data = EntityUtils.toString(response.getEntity());

            Log.e("DATAAA", data);


            return data;
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return "Slow";
        } catch (ConnectTimeoutException e) {
            e.printStackTrace();
            return "Slow";
        } catch (NullPointerException e) {
            e.printStackTrace();
            return "null";
        } catch (Exception e) {
            e.printStackTrace();
            return "Slow";
        }
    }


    public static void showAlert(final Context context, String title, String msg) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        Intent fragment_activityy = new Intent(context, FragmentActivity.class);
                        fragment_activityy.putExtra("fragment", "LOGIN");
                        context.startActivity(fragment_activityy);

                    }
                })
                /*.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })*/
                .setIcon(R.mipmap.ic_alert)
                .show();
    }

    public static void showAlert2(final Fragment context, String title, String msg) {
        new AlertDialog.Builder(context.getActivity())
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete


                    }
                })
                /*.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })*/
                .setIcon(R.mipmap.ic_alert)
                .show();
    }

    public static void showAlert2(final Context context, String title, String msg) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete


                    }
                })
                /*.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })*/
                .setIcon(R.mipmap.ic_alert)
                .show();
    }

    public static void showAlert3(final Fragment context, String title, String msg) {
        new AlertDialog.Builder(context.getActivity())
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete


                        Intent intent = new Intent(context.getActivity(), HomePage.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK  );

                        context.startActivity(intent);

                        context.getActivity().finish();

                    }
                })
                /*.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })*/
                .setIcon(R.mipmap.ic_alert)
                .show();
    }


    public static double calculatePrice(RealmResults<DemoClass> realmResults) {
        double sum = 0;
        for (int i = 0; i < realmResults.size(); i++) {
            String qty_str[] = realmResults.get(i).getCartProductQuantity().split(":");
            String price_str[] = realmResults.get(i).getCartProductPrice().split(":");
            double qty = Double.parseDouble(qty_str[0]);
            double price = Double.parseDouble(price_str[0]);
            sum += price * qty;
        }
        return sum;
    }

}
