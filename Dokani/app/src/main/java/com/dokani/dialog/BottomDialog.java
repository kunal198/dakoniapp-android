package com.dokani.dialog;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.dokani.Activity.FragmentActivity;
import com.dokani.Bean.DemoClass;
import com.dokani.CommonUtils.Constant;
import com.dokani.R;

import io.realm.Realm;
import io.realm.RealmResults;


public  class BottomDialog extends BottomSheetDialog implements View.OnClickListener{

TextView checkoutGuestTV,cancelTV,loginTV;
Context context;
    Realm realm;
    public BottomDialog(@NonNull Context context, Realm realm) {
        super(context);
        this.context=context;
        this.realm=realm;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.bottom_dialog);

        loginTV= (TextView) findViewById(R.id.loginTV);
        checkoutGuestTV= (TextView) findViewById(R.id.checkoutGuestTV);
        cancelTV= (TextView) findViewById(R.id.cancelTV);


        loginTV.setOnClickListener(BottomDialog.this);
        checkoutGuestTV.setOnClickListener(BottomDialog.this);
        cancelTV.setOnClickListener(BottomDialog.this);



    }


     @Override
     public void onClick(View view) {

         switch(view.getId())
         {
             case R.id.checkoutGuestTV:
                 RealmResults<DemoClass> results = realm.where(DemoClass.class).
                         contains("cartProductId","id")
                         .or()
                         .contains("cartProductPrice","price")
                         .or()
                         .contains("cartProductQuantity","qty")
                         .findAll();
                 double subTotal= Constant.calculatePrice(results);
                 String arr[] = results.get(0).getCartProductId().split(":");
                 Log.d("idddddd","---"+results.size());
                 Intent fragment_activityy = new Intent(context, FragmentActivity.class);
                 fragment_activityy.putExtra("fragment", "CHECK_OUT_SEARCH");
                 fragment_activityy.putExtra("product_id",arr[0]);
                 fragment_activityy.putExtra("qty",""+results.size());
                 fragment_activityy.putExtra("subtotal",""+subTotal);
                 context.startActivity(fragment_activityy);
                 dismiss();
                 break;
             case R.id.cancelTV:
                 dismiss();
                 break;
             case R.id.loginTV:
                 Intent login = new Intent(context, FragmentActivity.class);
                 login.putExtra("fragment", "LOGIN");
                 context.startActivity(login);
                 dismiss();
                 break;
         }

     }
 }