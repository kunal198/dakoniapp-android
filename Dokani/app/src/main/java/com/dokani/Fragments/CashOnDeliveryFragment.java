package com.dokani.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dokani.Activity.PaymentActivity;
import com.dokani.Async_Thread.Super_AsyncTask;
import com.dokani.Async_Thread.Super_AsyncTask_Interface;
import com.dokani.Bean.DemoClass;
import com.dokani.CommonUtils.Constant;
import com.dokani.R;
import com.dokani.RealmDataBase.RealmController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.realm.Realm;
import io.realm.RealmResults;


public class CashOnDeliveryFragment extends Fragment implements View.OnClickListener{


    Realm realm;
    HashMap hashMap;
    TextView payOnDeliveryTV;
    String from_cart="";
    public CashOnDeliveryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.cash_on_delivery, container, false);
        Bundle bundle=getArguments();
        if (bundle!=null) {
            hashMap= (HashMap) bundle.getSerializable("all_data");
            from_cart=  bundle.getString("from_cart");

        }
        initView(view);



        return view;
    }

    private void initView(View view)
    {
        realm= RealmController.with(getActivity()).getRealm();
        ((PaymentActivity)getActivity()).checkoutFrame.setVisibility(View.VISIBLE);
        ((PaymentActivity)getActivity()).paymentFrame.setVisibility(View.GONE);
        payOnDeliveryTV= (TextView) view.findViewById(R.id.payOnDeliveryTV);
        payOnDeliveryTV.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch(view.getId())
        {
            case R.id.payOnDeliveryTV:
                if (Constant.haveNetworkConnection(getActivity())) {

                    Log.d("hashhhhamap", "=" + hashMap);
                    Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.PAYMENT_API, new Super_AsyncTask_Interface() {
                        @Override
                        public void onTaskCompleted(String result) {
                            Log.d("hashhhhamap_result", "=" + result);
                            try {
                                JSONObject jsonObject=new JSONObject(result);

                                if (jsonObject.optString("success").equals("true"))
                                {
                                    Log.d("from_cart","--"+from_cart);
                                    if (from_cart.equals("yes"))
                                    {
                                        RealmResults<DemoClass> resultss = realm.where(DemoClass.class).
                                                contains("cartProductName","name")
                                                .or()
                                                .contains("cartProductUrl","url")
                                                .or()
                                                .contains("cartProductPrice","price")
                                                .or()
                                                .contains("cartProductId","id")
                                                .or()
                                                .contains("cartProductRating","rating")
                                                .or()
                                                .contains("cartProductQuantity","qty")
                                                .findAll();
                                        realm.beginTransaction();
                                        resultss.clear();
                                        realm.commitTransaction();
                                    }



                                    Constant.showAlert3(CashOnDeliveryFragment.this, getResources().getString(R.string.alert), getResources().getString(R.string.ur_order_no)+jsonObject.optString("order_id")+ getResources().getString(R.string.placed_success));


                                }
                                else if (jsonObject.optString("success").equals("false"))
                                {

                                    Constant.showAlert3(CashOnDeliveryFragment.this, getResources().getString(R.string.alert), jsonObject.optString("message"));

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }



                        }
                    }, true, false));
                }

                break;
        }
    }



}
