package com.dokani.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dokani.Activity.FragmentActivity;
import com.dokani.Adapters.CategoryPagerAdapter;
import com.dokani.Adapters.ImagePagerAdapter;
import com.dokani.Adapters.NewArrivalAdapter;
import com.dokani.Async_Thread.Super_AsyncTask;
import com.dokani.Async_Thread.Super_AsyncTask_Interface;
import com.dokani.Bean.DemoClass;
import com.dokani.Bean.HomePageBean;
import com.dokani.Classes.AutoScrollViewPager;
import com.dokani.CommonUtils.Constant;
import com.dokani.CustomViewPager.MyScrollView;
import com.dokani.R;
import com.dokani.RealmDataBase.RealmController;
import com.dokani.utils.MyUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import me.relex.circleindicator.CircleIndicator;


public class HomePageFragment extends Fragment implements View.OnClickListener {

    private Realm realm;

    private CircleIndicator circleIndicator;
    private CategoryPagerAdapter categoryPagerAdapter;
    private NewArrivalAdapter newArrivalAdapter;
    private MyScrollView categoryListView, newArrivalMV;
    private AutoScrollViewPager mainVP;
    private ImageView dealIV, categoryLeftArrow, categoryRightArrow, newArrivalLeftArrow, newArrivalRightArrow;
    private LinearLayout categoryLL;
    private ArrayList<String> banner_list;
    ArrayList<HomePageBean> data_list;
    public HomePageFragment() {
        // Required empty public constructor

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        HomePage.titleTV.setText("Home");
        this.realm = RealmController.with(this).getRealm();
        Constant.hideKeyboard(getActivity());
        View view = inflater.inflate(R.layout.home_page_fragment_layout, container, false);

        initLists();
        initViews(view);

        //getStoredCategoryData();
        Log.d("check_net", "" + MyUtils.myInstance().checkConnection());
        return view;
    }

    private void initLists() {
        banner_list = new ArrayList<>();
        data_list=new ArrayList<>();
    }

    private void initViews(View view) {
        mainVP = (AutoScrollViewPager) view.findViewById(R.id.mainVP);
        categoryLL = (LinearLayout) view.findViewById(R.id.categoryLL);
        newArrivalMV = (MyScrollView) view.findViewById(R.id.newArrivalMV);
        categoryListView = (MyScrollView) view.findViewById(R.id.categoryListView);
        dealIV = (ImageView) view.findViewById(R.id.dealIV);
        circleIndicator = (CircleIndicator) view.findViewById(R.id.circleIndicator);
        categoryLeftArrow = (ImageView) view.findViewById(R.id.categoryLeftArrow);
        categoryRightArrow = (ImageView) view.findViewById(R.id.categoryRightArrow);
        newArrivalLeftArrow = (ImageView) view.findViewById(R.id.newArrivalLeftArrow);
        newArrivalRightArrow = (ImageView) view.findViewById(R.id.newArrivalRightArrow);
        attachListeners();

        setDealView();
        setupHorizonalListView(categoryListView);
        setupNewArrivalHorizontalListView(newArrivalMV);

        if (getStoredCategoryDataSize() == 0) {
            if (MyUtils.myInstance().checkConnection()) {

                getcategories();
                getBanner();
                getNewArrivalProducts();
            }

        } else {
            getStoredCategoryData();
            getStoredBannerDataSize();
            getStoredNewArrivalData();
            getcategories();
            getBanner();
            getNewArrivalProducts();

        }
    }

    private void attachListeners() {
        categoryRightArrow.setOnClickListener(this);
        categoryLeftArrow.setOnClickListener(this);
        newArrivalLeftArrow.setOnClickListener(this);
        newArrivalRightArrow.setOnClickListener(this);
        categoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent fragment_activityy = new Intent(getActivity(), FragmentActivity.class);
                Log.d("position", "" + i);
                fragment_activityy.putExtra("fragment", "SUBCATEGORY");
                fragment_activityy.putExtra("Position", i);
                startActivity(fragment_activityy);
            }
        });

    }

    private void setupNewArrivalHorizontalListView(final MyScrollView newArrivalMV) {
         newArrivalAdapter = new NewArrivalAdapter(getActivity(), data_list);
        newArrivalMV.setAdapter(newArrivalAdapter);


    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void setupMainViewPager(AutoScrollViewPager viewPager, CircleIndicator circleIndicator, ArrayList <String>banner_list,int layout) {


        ImagePagerAdapter imagePagerAdapter = new ImagePagerAdapter(getActivity(), banner_list,layout);
        viewPager.setAdapter(imagePagerAdapter);
        circleIndicator.setViewPager(viewPager);
        viewPager.startAutoScroll(2000);
        viewPager.setStopScrollWhenTouch(true);
        viewPager.setAutoScrollDurationFactor(17);

    }

    private void setupHorizonalListView(final MyScrollView horizontalListView) {

        categoryPagerAdapter = new CategoryPagerAdapter(getActivity(), Constant.categoryImageData(), Constant.category_list);
        horizontalListView.setAdapter(categoryPagerAdapter);


    }

    private void setDealView() {
        dealIV.setImageResource(R.mipmap.banner2);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.categoryRightArrow:
                categoryListView.moveRight();

                break;
            case R.id.categoryLeftArrow:
                categoryListView.moveLeft();

                break;
            case R.id.newArrivalLeftArrow:
                newArrivalMV.moveLeft();
                break;
            case R.id.newArrivalRightArrow:
                newArrivalMV.moveRight();
                break;
        }

    }

    private void getBanner() {
        if (Constant.haveNetworkConnection(getActivity())) {
            Constant.execute(new Super_AsyncTask(getActivity(), Constant.BANNER_API, new Super_AsyncTask_Interface() {

                @Override
                public void onTaskCompleted(String output) {
                    if (realm.where(DemoClass.class).equalTo("id", 01).findFirst() == null) {

                        DemoClass demoClass = new DemoClass();
                        demoClass.setId(01);
                        demoClass.setBannerData(output);
                        Constant.copyDataToRealm(realm, demoClass);
                        getStoredBannerDataSize();
                    } else {
                        realm.beginTransaction();
                        DemoClass demoClass = realm.where(DemoClass.class)
                                .equalTo("id", 01).findFirst();
                        demoClass.setBannerData(output);
                        realm.commitTransaction();
                        getStoredBannerDataSize();
                    }


                    Log.d("output-----", "" + output);
                    Log.d("output-----", "out");

                }


            }, false, false));
        }

    }  private void getNewArrivalProducts() {
        if (Constant.haveNetworkConnection(getActivity())) {
            Constant.execute(new Super_AsyncTask(getActivity(), Constant.NEW_ARRIVAL_PRODUCT_API, new Super_AsyncTask_Interface() {

                @Override
                public void onTaskCompleted(String output) {
                    Log.d("newArrival", "data" + output);
                    if (realm.where(DemoClass.class).equalTo("id", 05).findFirst() == null) {

                        DemoClass demoClass = new DemoClass();
                        demoClass.setId(05);
                        demoClass.setNewArrivalData(output);
                        Constant.copyDataToRealm(realm, demoClass);
                        getStoredNewArrivalData();
                    } else {
                        realm.beginTransaction();
                        DemoClass demoClass = realm.where(DemoClass.class)
                                .equalTo("id", 05).findFirst();
                        demoClass.setNewArrivalData(output);
                        realm.commitTransaction();
                        getStoredNewArrivalData();
                    }


                }

            }, false, false));
        }

    }

    private void getStoredBannerDataSize() {
        DemoClass demoClass = realm.where(DemoClass.class).equalTo("id", 01)
                .findFirst();
        if (demoClass.getBannerData() != null) {


            if (banner_list != null)
                banner_list.clear();
            Log.d("banner_data", "" + demoClass.getBannerData());


            try {
                JSONArray   jsonArray = new JSONArray(demoClass.getBannerData());

            Log.d("banner_data_json", "" + jsonArray);
            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.optJSONObject(i);
                String slide_id = jsonObject.optString("slide_id");
                JSONArray jsonArray1 = null;
                try {
                    jsonArray1 = new JSONArray(slide_id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for (int j = 0; j < jsonArray1.length(); j++) {
                    JSONObject jsonObject1 = jsonArray1.optJSONObject(j);
                    banner_list.add(jsonObject1.optString("slide_image"));
                }
                break;
            }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            setupMainViewPager(mainVP, circleIndicator, banner_list,R.layout.pager_item);
        }
    }

    private void getcategories() {
        if (Constant.haveNetworkConnection(getActivity())) {
            Constant.execute(new Super_AsyncTask(getActivity(), Constant.CATEGORY_API, new Super_AsyncTask_Interface() {

                @Override
                public void onTaskCompleted(String output) {
                    Log.d("outputt-->", "Category_output" + output);
                    if (realm.where(DemoClass.class).equalTo("id", 02).findFirst() == null) {
                        DemoClass demoClass = new DemoClass();
                        demoClass.setId(02);
                        demoClass.setCategoryData(output);
                        Constant.copyDataToRealm(realm, demoClass);
                        getStoredCategoryData();
                    } else {
                        realm.beginTransaction();
                        DemoClass demoClass = realm.where(DemoClass.class)
                                .equalTo("id", 02).findFirst();
                        demoClass.setCategoryData(output);
                        realm.commitTransaction();
                        getStoredCategoryData();
                        Log.d("output-----", "");
                        Log.d("output-----", "out----");
                    }


                }

            }, false, false));

        }
    }

    private void getStoredCategoryData() {
        DemoClass demoClass = realm.where(DemoClass.class).equalTo("id", 02)
                .findFirst();

        Log.d("resultss--", "" + demoClass.getCategoryData());
        if (demoClass.getCategoryData() != null) {


            Constant.category_list.clear();

            try {
                JSONArray jsonArray  = new JSONArray(demoClass.getCategoryData());

            for (int i = 0; i < jsonArray.length(); i++) {

                Constant.category_list.add(jsonArray.optJSONObject(i).optString("category_name"));

            }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            categoryPagerAdapter.notifyDataSetChanged();
        }
    }
    private void getStoredNewArrivalData() {


        DemoClass demoClass = realm.where(DemoClass.class).equalTo("id", 05)
                .findFirst();

        Log.d("resultss------==", "" + demoClass.getNewArrivalData());
        if (demoClass.getNewArrivalData() != null) {

            try {

                data_list.clear();
                JSONArray jsonArray = new JSONArray(demoClass.getNewArrivalData());
                for (int i=0;i<jsonArray.length();i++)
                {
                    HomePageBean homePageBean=new HomePageBean();

                    JSONObject jsonObject=jsonArray.optJSONObject(i);
                    homePageBean.setProductName(jsonObject.optString("product_name"));
                    homePageBean.setProductPrice(jsonObject.optString("price"));
                    homePageBean.setProductId(jsonObject.optString("product_id"));
                    homePageBean.setProductUrl(jsonObject.optString("image"));
                    data_list.add(homePageBean);

                }


                Log.d("output-----","out");

            } catch (JSONException e) {
                e.printStackTrace();
            }

           newArrivalAdapter.notifyDataSetChanged();
            newArrivalMV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent fragment_activityy = new Intent(getActivity(), FragmentActivity.class);
                    fragment_activityy.putExtra("fragment", "PRODUCTS_DETAILS");
                    fragment_activityy.putExtra("product_id",data_list.get(i).getProductId());
                    startActivity(fragment_activityy);
                }
            });


        }
    }

    private int getStoredCategoryDataSize() {


        RealmResults<DemoClass> results = realm.where(DemoClass.class).findAll();
        Log.d("Log_tag", "results" + results);
        return results.size();

    }
////For New Arrivals Products


}
