package com.dokani.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.dokani.Activity.FragmentActivity;
import com.dokani.Adapters.SearchListAdapter;
import com.dokani.Adapters.SortSpinnerAdapter;
import com.dokani.Async_Thread.Super_AsyncTask;
import com.dokani.Async_Thread.Super_AsyncTask_Interface;
import com.dokani.Bean.HomePageBean;
import com.dokani.CommonUtils.Constant;
import com.dokani.R;
import com.dokani.RealmDataBase.RealmController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import io.realm.Realm;

public class ProductFragment extends Fragment implements View.OnClickListener {


    private Spinner sortBySP;
    private ListView productsLV;
    RelativeLayout noDataLayoutRLl,filterByRL;
    private ImageView searchCheckoutIV;
    private  ArrayList<HomePageBean> data_list;
    SearchListAdapter searchListAdapter;
    String page_number="0";
    int total_pages=1;
    String filter_output="";
    Realm realm;
        String cat_id= "";
    public ProductFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//        HomePage.titleTV.setText("Search Results");
        // Inflate the layout for this fragment
        Constant.hideKeyboard(getActivity());
        realm= RealmController.with(getActivity()).getRealm();
        Bundle bundle=getArguments();
        if (bundle!=null)
        {
            cat_id=getArguments().getString("cat_id");
            filter_output=getArguments().getString("output");
        }


        View view = inflater.inflate(R.layout.products_list_main, container, false);

        initView(view);

        return view;
    }

    private void initView(View view) {
        productsLV = (ListView) view.findViewById(R.id.productsLV);
        noDataLayoutRLl = (RelativeLayout) view.findViewById(R.id.noDataLayoutRL);
        filterByRL= (RelativeLayout) view.findViewById(R.id.filterByRL);
        sortBySP= (android.widget.Spinner) view.findViewById(R.id.sortBySP);

        filterByRL.setOnClickListener(this);
        data_list=new ArrayList<HomePageBean>();



        searchListAdapter = new SearchListAdapter(getActivity(), data_list,realm);
        productsLV.setAdapter(searchListAdapter);
        productsLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent fragment_activityy = new Intent(getActivity(), FragmentActivity.class);
                fragment_activityy.putExtra("fragment", "PRODUCTS_DETAILS");
                fragment_activityy.putExtra("product_id",data_list.get(i).getProductId());
                startActivity(fragment_activityy);
            }
        });
        productsLV.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
             //   Log.d("absListView","onScrollStateChanged"+absListView);
                if (productsLV.getLastVisiblePosition() == productsLV.getAdapter().getCount() -1 &&
                        productsLV.getChildAt(productsLV.getChildCount() - 1).getBottom() <= productsLV.getHeight())
                {
                    //It is scrolled all the way down here
                    Log.d("absListView","onScroll"+i+"==");
                    if (filter_output==null)
                    getProducts(false);
                }

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
             //   Log.d("absListView","onScroll"+i+"=="+i1+"=="+i2);



            }
        });

        if (filter_output!=null)
        {
            Log.d("data_list_size","--"+data_list.size());
            setFilteredProducts(filter_output);
        }
        else{
            getProducts(true);
        }
        setSppiner();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.filterByRL:
                if (noDataLayoutRLl.getVisibility()==View.GONE)
                {
                FilterFragment filterFragment=new FilterFragment();
                Bundle bundle=new Bundle();
                bundle.putString("cat_id",cat_id);
                Log.d("cat_id","-"+cat_id);
                filterFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragmentsContainer, filterFragment)
                        .commit();
                }
                else{
                    Constant.showAlert2(getActivity(),getResources().getString(R.string.alert),getResources().getString(R.string.filter_not_found));
                }
                break;

            default:
                break;
        }


    }

    private void setSppiner()
    {
        ArrayList<String> categories = new ArrayList<String>();
        categories.add(getResources().getString(R.string.sort_by));
        categories.add(getResources().getString(R.string.low_to_high));
        categories.add(getResources().getString(R.string.hifh_to_low));
        categories.add(getResources().getString(R.string.a_z));
        categories.add(getResources().getString(R.string.z_a));
        categories.add(getResources().getString(R.string.recent));
        categories.add(getResources().getString(R.string.popular));
        // Creating adapter for spinner
        SortSpinnerAdapter dataAdapter = new SortSpinnerAdapter(getActivity(), R.layout.spinner_layout,categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        sortBySP.setAdapter(dataAdapter);
        sortBySP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String item = adapterView.getItemAtPosition(i).toString();
                Log.d("itemmm","--"+item
                +"---"+i);
                String attribute[]={"","price","price","name","name"};
                String order[]={"","asc","desc","asc","desc"};
                if (i<5 &&i>0)
                getSortedProducts(attribute[i],order[i]);
                else if (i==5)
                    getRecentProducts();
                else if (i==6)
                    getPopularProducts();




            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }



    private void setFilteredProducts(String output ) {


                try {
                    Log.d("hellooooo", "1" );
                    Log.d("setFilteredProducts", "" + output);
                   // data_list.clear();
                    JSONObject jsonObject1 = new JSONObject(output);
                  //  ArrayList<HomePageBean> data_list=new ArrayList<>();

                        JSONArray prod_array= jsonObject1.optJSONArray("products");
                        if (prod_array.length()<1)
                        {
                            Log.d("output-----", "out-------------");
                            noDataLayoutRLl.setVisibility(View.VISIBLE);
                            productsLV.setVisibility(View.GONE);
                            searchListAdapter.notifyDataSetChanged();
                        }
                        else {
                            noDataLayoutRLl.setVisibility(View.GONE);
                            productsLV.setVisibility(View.VISIBLE);
                            for (int k = 0; k < prod_array.length(); k++) {
                                JSONObject jsonObject = prod_array.optJSONObject(k);
                                HomePageBean homePageBean = new HomePageBean();
                                homePageBean.setProductName(jsonObject.optString("product_name"));

                                if (jsonObject.optString("special_price").equals("")) {
                                    homePageBean.setProductPrice(jsonObject.optString("product_price"));
                                } else {
                                    homePageBean.setProductPrice(jsonObject.optString("special_price"));
                                }
                                homePageBean.setProductSmallDesc(jsonObject.optString("description"));
                                homePageBean.setProductUrl(jsonObject.optString("product_image_url"));
                                homePageBean.setProductId(jsonObject.optString("product_id"));
                                homePageBean.setProductRating(jsonObject.optString("product_rating"));
                                data_list.add(homePageBean);
                            }
                            searchListAdapter.notifyDataSetChanged();


                        }





                    Log.d("output-----", "" + output);
                    Log.d("output-----", "out"+data_list.size());

                } catch (JSONException e) {
                    e.printStackTrace();
                }



    }
    private void getPopularProducts() {

        HashMap<String, String> hashMap = new HashMap();
        hashMap.put("category_id", cat_id);
        hashMap.put("page_number", "1");
        if (Constant.haveNetworkConnection(getActivity())) {
            Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.POPULAR_PRODUCTS, new Super_AsyncTask_Interface() {

                @Override
                public void onTaskCompleted(String output) {
                    try {

                        Log.d("hellooooo", "2");
                        data_list.clear();
                        Log.d("getPopularProducts", "" + output);

                        JSONArray jsonArray = new JSONArray(output);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.optJSONObject(i);

                            JSONArray prod_array = jsonObject1.optJSONArray("products");
                            if (prod_array.length() < 1) {
                                Log.d("output-----", "out-------------");
                                noDataLayoutRLl.setVisibility(View.VISIBLE);
                                productsLV.setVisibility(View.GONE);
                            } else {
                                noDataLayoutRLl.setVisibility(View.GONE);
                                productsLV.setVisibility(View.VISIBLE);
                                for (int k = 0; k < prod_array.length(); k++) {
                                    JSONObject jsonObject = prod_array.optJSONObject(k);
                                    HomePageBean homePageBean = new HomePageBean();
                                    homePageBean.setProductName(jsonObject.optString("product_name"));

                                    if (jsonObject.optString("special_price").equals("")) {
                                        homePageBean.setProductPrice(jsonObject.optString("product_price"));
                                    } else {
                                        homePageBean.setProductPrice(jsonObject.optString("special_price"));
                                    }
                                    homePageBean.setProductSmallDesc(jsonObject.optString("description"));
                                    homePageBean.setProductUrl(jsonObject.optString("product_image_url"));
                                    homePageBean.setProductId(jsonObject.optString("product_id"));
                                    homePageBean.setProductRating(jsonObject.optString("product_rating"));
                                    data_list.add(homePageBean);
                                }

                            }


                        }

                        searchListAdapter.notifyDataSetChanged();
                        Log.d("output-----", "" + output);
                        Log.d("output-----", "out");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }, true, false));
        }
    }


    private void getRecentProducts() {

        HashMap<String, String> hashMap = new HashMap();
        hashMap.put("category_id", cat_id);
        hashMap.put("page_number", "1");
        if (Constant.haveNetworkConnection(getActivity())) {
            Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.RECENT_PRODUCTS, new Super_AsyncTask_Interface() {

                @Override
                public void onTaskCompleted(String output) {
                    try {

                        Log.d("hellooooo", "3");
                        data_list.clear();
                        Log.d("getPopularProducts", "" + output);

                        JSONArray jsonArray = new JSONArray(output);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.optJSONObject(i);

                            JSONArray prod_array = jsonObject1.optJSONArray("products");
                            if (prod_array.length() < 1) {
                                Log.d("output-----", "out-------------");
                                noDataLayoutRLl.setVisibility(View.VISIBLE);
                                productsLV.setVisibility(View.GONE);
                            } else {
                                noDataLayoutRLl.setVisibility(View.GONE);
                                productsLV.setVisibility(View.VISIBLE);
                                for (int k = 0; k < prod_array.length(); k++) {
                                    JSONObject jsonObject = prod_array.optJSONObject(k);
                                    HomePageBean homePageBean = new HomePageBean();
                                    homePageBean.setProductName(jsonObject.optString("product_name"));

                                    if (jsonObject.optString("special_price").equals("")) {
                                        homePageBean.setProductPrice(jsonObject.optString("product_price"));
                                    } else {
                                        homePageBean.setProductPrice(jsonObject.optString("special_price"));
                                    }
                                    homePageBean.setProductSmallDesc(jsonObject.optString("description"));
                                    homePageBean.setProductUrl(jsonObject.optString("product_image_url"));
                                    homePageBean.setProductId(jsonObject.optString("product_id"));
                                    homePageBean.setProductRating(jsonObject.optString("product_rating"));
                                    data_list.add(homePageBean);
                                }
                            }


                        }

                        searchListAdapter.notifyDataSetChanged();
                        Log.d("output-----", "" + output);
                        Log.d("output-----", "out");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }


            }, true, false));
        }
    }
 /*   product_name,product_id,product_image_url,description,product_price,special_price,product_rating*/
    private void getProducts(final boolean needToClearArrayList)
    {

        int page_num=Integer.parseInt(page_number)+1;
        Log.d("page_no","--"+page_num+"=="+total_pages);
        if (page_num<=total_pages)
        {

        HashMap<String, String> hashMap = new HashMap();
        hashMap.put("category_id", cat_id);
        Log.d("catt_id","--"+cat_id);

        hashMap.put("page_number", String.valueOf(page_num));
        if (Constant.haveNetworkConnection(getActivity())) {
            Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.GET_PRODUCT_FOR_CATEGORY_API, new Super_AsyncTask_Interface() {

                @Override
                public void onTaskCompleted(String output) {
                    try {
                        Log.d("resultsOfproducts", "" + output);

                        Log.d("hellooooo", "4");
                        if (needToClearArrayList)
                        {
                            data_list.clear();

                        }
                        JSONArray jsonArray = new JSONArray(output);


                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            total_pages=jsonObject.optInt("total_pages");
                            page_number=jsonObject.optString("current_page");
                            JSONArray jsonArray1 = jsonObject.optJSONArray("products");
                            Log.d("jsonarray", "--" + jsonArray1);
                            if (jsonArray1.length() < 1) {
                                Log.d("output-----", "out-------------");
                                noDataLayoutRLl.setVisibility(View.VISIBLE);
                                productsLV.setVisibility(View.GONE);
                            } else {
                                for (int k = 0; k < jsonArray1.length(); k++) {
                                    noDataLayoutRLl.setVisibility(View.GONE);
                                    productsLV.setVisibility(View.VISIBLE);
                                    JSONObject jsonObject1 = jsonArray1.optJSONObject(k);
                                    HomePageBean homePageBean = new HomePageBean();
                                    homePageBean.setProductName(jsonObject1.optString("product_name"));


                                    if (jsonObject1.optString("special_price").equals("")) {
                                        homePageBean.setProductPrice(jsonObject1.optString("product_price"));
                                    } else {
                                        homePageBean.setProductPrice(jsonObject1.optString("special_price"));
                                    }
                                    homePageBean.setProductUrl(jsonObject1.optString("product_image_url"));
                                    homePageBean.setProductId(jsonObject1.optString("product_id"));
                                    homePageBean.setProductAvailableQty(jsonObject1.optString("qty"));
                                    homePageBean.setProductSmallDesc(jsonObject1.optString("description"));
                                    homePageBean.setProductRating(jsonObject1.optString("product_rating"));
                                    data_list.add(homePageBean);
                                }
                            }


                        }

                        searchListAdapter.notifyDataSetChanged();
                        Log.d("output-----", "" + output);
                        Log.d("output-----", "out");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }, true, false));

        }

        }
    }
    private void getSortedProducts(String attribute_type,String sort_order)
    {
        HashMap<String, String> hashMap = new HashMap();
        hashMap.put("category_id", cat_id);
        hashMap.put("page_number", "1");
        hashMap.put("sort_order", sort_order);
        hashMap.put("attribute_code", attribute_type);
        if (Constant.haveNetworkConnection(getActivity())) {
            Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.SORT_PRODUCTS, new Super_AsyncTask_Interface() {

                @Override
                public void onTaskCompleted(String output) {
                    try {

                        data_list.clear();
                        JSONArray jsonArray = new JSONArray(output);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            JSONArray jsonArray1 = jsonObject.optJSONArray("products");
                            Log.d("jsonarray", "--" + jsonArray1);
                            if (jsonArray1.length() < 1) {
                                Log.d("output-----", "out-------------");
                                noDataLayoutRLl.setVisibility(View.VISIBLE);
                                productsLV.setVisibility(View.GONE);
                            } else {
                                for (int k = 0; k < jsonArray1.length(); k++) {
                                    noDataLayoutRLl.setVisibility(View.GONE);
                                    productsLV.setVisibility(View.VISIBLE);
                                    JSONObject jsonObject1 = jsonArray1.optJSONObject(k);
                                    HomePageBean homePageBean = new HomePageBean();
                                    homePageBean.setProductName(jsonObject1.optString("product_name"));
                                    if (jsonObject1.optString("special_price").equals("")) {
                                        homePageBean.setProductPrice(jsonObject1.optString("product_price"));
                                    } else {
                                        homePageBean.setProductPrice(jsonObject1.optString("special_price"));
                                    }
                                    homePageBean.setProductSmallDesc(jsonObject1.optString("description"));
                                    homePageBean.setProductUrl(jsonObject1.optString("product_image_url"));
                                    homePageBean.setProductId(jsonObject1.optString("product_id"));
                                    homePageBean.setProductRating(jsonObject1.optString("product_rating"));
                                    data_list.add(homePageBean);
                                }


                            }
                        }

                        searchListAdapter.notifyDataSetChanged();
                        Log.d("output-----", "" + output);
                        Log.d("output-----", "out");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }, false, false));

        }
    }

}
