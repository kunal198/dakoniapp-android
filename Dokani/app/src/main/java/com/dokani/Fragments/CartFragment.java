package com.dokani.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dokani.Activity.FragmentActivity;
import com.dokani.Activity.HomePage;
import com.dokani.Adapters.CartAdpater;
import com.dokani.Bean.Cartbean;
import com.dokani.Bean.DemoClass;
import com.dokani.CommonUtils.Constant;
import com.dokani.MySharedPreferences;
import com.dokani.R;
import com.dokani.RealmDataBase.RealmController;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class CartFragment extends Fragment implements View.OnClickListener {
   // public static  ListView mycartLV;
    private RecyclerView mycartLV;
    TextView proceedToCheckoutTV,continueShoppingTV;

    public LinearLayout dataLayoutLL;
    public RelativeLayout noDataLayoutRL;
    public static CartAdpater cartAdapter;
    private ArrayList<Cartbean> quantity=new ArrayList<>();
    private Realm realm;
    View view;
    boolean isFragmentVisible=false;
    public CartFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("setMenuVisibility","onCreateView");
        Constant.hideKeyboard(getActivity());
         view = inflater.inflate(R.layout.my_cart_layout, container, false);
        initView(view);

        isFragmentVisible=true;

        return view;
        // Inflate the layout for this fragment

    }



    private void initView(View view) {
        realm= RealmController.with(getActivity()).getRealm();
        proceedToCheckoutTV= (TextView) view.findViewById(R.id.proceedToCheckoutTV);
        continueShoppingTV= (TextView) view.findViewById(R.id.continueShoppingTV);
        dataLayoutLL= (LinearLayout) view.findViewById(R.id.dataLayoutLL);
        noDataLayoutRL= (RelativeLayout) view.findViewById(R.id.noDataLayoutRL);
        proceedToCheckoutTV.setOnClickListener(this);
        continueShoppingTV.setOnClickListener(this);
        setUpListViewAdapter();
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        Log.d("setMenuVisibility","setMenuVisibility"+menuVisible);
        if (isFragmentVisible&&menuVisible)
        {
            setUpListViewAdapter();
          // cartAdapter.notifyDataSetChanged();
        }
        else{

        }
    }




    public void setUpListViewAdapter() {

        RealmResults<DemoClass> results = realm.where(DemoClass.class).
                contains("cartProductName","name")
                .or()
                .contains("cartProductUrl","url")
                .or()
                .contains("cartProductPrice","price")
                .or()
                .contains("cartProductId","id")
                .or()
                .contains("cartProductRating","rating")
                .or()
                .contains("cartProductQuantity","qty")
                .or()
                .contains("cartProductAvailabilityQty","qty")
                .findAll();

           for (int i=0;i<results.size();i++)
           {
               results.get(i).getCartProductPrice();
           }
        if (results.size()==0)
        {
            noDataLayoutRL.setVisibility(View.VISIBLE);
            dataLayoutLL.setVisibility(View.GONE);

        }
       else {
            dataLayoutLL.setVisibility(View.VISIBLE);
            noDataLayoutRL.setVisibility(View.GONE);
            mycartLV = (RecyclerView) view.findViewById(R.id.mycartLV);
            cartAdapter = new CartAdpater(getActivity(), results,realm,CartFragment.this,mycartLV);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            mycartLV.setLayoutManager(mLayoutManager);
            mycartLV.setItemAnimator(new DefaultItemAnimator());
            mycartLV.setAdapter(cartAdapter);
        }


    }


    @Override
    public void onClick(View view) {

        switch(view.getId())
        {
            case R.id.proceedToCheckoutTV:
               String session= MySharedPreferences.getInstance().getData(getActivity(),"session_id");
                if (session.equals("null"))
                {
                    askForLogin();
                }
                else{
                    doTransaction();

                }
                break;
            case R.id.continueShoppingTV:
                HomePage.viewpager.setCurrentItem(0);
                break;
        }

    }



    private void askForLogin() {

        final String[] stringItems = {getResources().getString(R.string.login),getResources().getString(R.string.checkout_guest)};
        final ActionSheetDialog dialog = new ActionSheetDialog(getActivity(), stringItems, null,getResources().getString(R.string.cancel));
        dialog.isTitleShow(false).show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {


                if (position == 0) {
                    Log.e("pos1", "dalsjhdjk");
                    Intent login = new Intent(getActivity(), FragmentActivity.class);
                    login.putExtra("fragment", "LOGIN");
                    startActivity(login);

                } else if (position == 1) {
                    doTransaction();


                }
                dialog.dismiss();

            }
        });
    }

    private void doTransaction()
    {
        RealmResults<DemoClass> results = realm.where(DemoClass.class).
                contains("cartProductId","id")
                .or()
                .contains("cartProductPrice","price")
                .or()
                .contains("cartProductQuantity","qty")
                .findAll();
        double subTotal=Constant.calculatePrice(results);
        Log.d("idddddd","---"+results.size());
        //String prod_arr[]=new String[results.size()];
        //String qty_arr[]=new String[results.size()];

        String prod_ids="";
        String quantitys="";
        for(int i=0;i<results.size();i++)
        {
            String id[]=results.get(i).getCartProductId().split(":");
            String qty[]=results.get(i).getCartProductQuantity().split(":");
            if (i<results.size()-1)
            {
                prod_ids=prod_ids+id[0]+",";
                quantitys=quantitys+qty[0]+",";
            }
            else{
                prod_ids=prod_ids+id[0];
                quantitys=quantitys+qty[0];
            }

        }
       // Log.d("prod_arrray","--"+prod_arr);
       // Log.d("qty_arrray","--"+qty_arr);
        Intent fragment_activityy = new Intent(getActivity(), FragmentActivity.class);
        fragment_activityy.putExtra("fragment", "CHECK_OUT_SEARCH");
        fragment_activityy.putExtra("product_id",prod_ids);
        fragment_activityy.putExtra("qty",quantitys);
        fragment_activityy.putExtra("qty_size",""+results.size());
        fragment_activityy.putExtra("subtotal",""+subTotal);
        fragment_activityy.putExtra("from_cart",1);
        startActivity(fragment_activityy);
    }
}
