package com.dokani.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.dokani.Async_Thread.Super_AsyncTask;
import com.dokani.Async_Thread.Super_AsyncTask_Interface;
import com.dokani.CommonUtils.Constant;
import com.dokani.Listeners.AlertDialogListener;
import com.dokani.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class SignUpFragment extends Fragment implements View.OnClickListener,AlertDialogListener {

    private TextView signUpTV;
    private EditText firstNameET, lastNameET, emailAddressET, phoneNoET, createPassET, confirmPassET;
   private AlertDialogListener alertDialogListener;
    public SignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Constant.hideKeyboard(getActivity());
        View view = inflater.inflate(R.layout.signup_user, container, false);
        initView(view);


        return view;
        // Inflate the layout for this fragment

    }

    private void initView(View view) {

        firstNameET = (EditText) view.findViewById(R.id.firstNameET);
        lastNameET = (EditText) view.findViewById(R.id.lastNameET);
        emailAddressET = (EditText) view.findViewById(R.id.emailAddressET);
        phoneNoET = (EditText) view.findViewById(R.id.phoneNoET);
        createPassET = (EditText) view.findViewById(R.id.createPassET);
        confirmPassET = (EditText) view.findViewById(R.id.confirmPassET);
        signUpTV = (TextView) view.findViewById(R.id.signUpTV);
        alertDialogListener=this;
        signUpTV.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.signUpTV:
                if (validationOfData()) {
                    HashMap<String, String> hashMap = new HashMap();
                    hashMap.put("firstname", firstNameET.getText().toString());
                    hashMap.put("lastname", lastNameET.getText().toString());
                    hashMap.put("email", emailAddressET.getText().toString());
                    hashMap.put("password", createPassET.getText().toString());
                    hashMap.put("mobile", phoneNoET.getText().toString());
                    // hashMap.put("devicetoken", token);
                    if (Constant.haveNetworkConnection(getActivity())) {
                        Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.REGISTER_WITH_EMAIL, new Super_AsyncTask_Interface() {

                            @Override
                            public void onTaskCompleted(String output) {
                                try {
                                    JSONObject jsonObject = new JSONObject(output);
                                    Log.d("signup", "" + output);
                                    Constant.showAlert(SignUpFragment.this, getResources().getString(R.string.alert), jsonObject.optString("message"));

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                        }, true, false));
                    }
                }

                break;
        }

    }

    private boolean validationOfData() {
        boolean status=true;
        if (!Constant.validateFields(firstNameET.getText().toString())) {
            firstNameET.setError(getResources().getString(R.string.please_enter_first_name));
            status=false;
        }
        if (!Constant.validateFields(lastNameET.getText().toString())) {
            lastNameET.setError(getResources().getString(R.string.please_enter_last_name));
            status=false;
        }
        if (!Constant.emailValidator(emailAddressET.getText().toString())) {
            emailAddressET.setError(getResources().getString(R.string.please_enter_email));
            status=false;
        }
        if (Constant.matchPassword(createPassET.getText().toString(), confirmPassET.getText().toString())) {
            confirmPassET.setError(getResources().getString(R.string.confirm_not_match));
            status=false;
        }
        if (!Constant.validatelength(createPassET.getText().toString())) {
            createPassET.setError(getResources().getString(R.string.pls_enter_eight_digit));
            status=false;
        }
        return status;
    }

    @Override
    public void onPressOk() {
        LoginFragment loginFragment=new LoginFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentsContainer, loginFragment).addToBackStack(null)
                .commit();
    }
}
