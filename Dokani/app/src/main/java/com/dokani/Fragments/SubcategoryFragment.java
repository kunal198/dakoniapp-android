package com.dokani.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListView;

import com.dokani.Activity.FragmentActivity;
import com.dokani.Adapters.ProductAdapter;
import com.dokani.Bean.DemoClass;
import com.dokani.CommonUtils.Constant;
import com.dokani.R;
import com.dokani.RealmDataBase.RealmController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import io.realm.Realm;

public class SubcategoryFragment extends Fragment implements View.OnClickListener,ExpandableListView.OnChildClickListener {
    private Realm realm;
  //  private ExpandableListView subcategoryEL;
  //  SubCategoryExpandableListAdapter expandableListAdapter;
    ListView productRV;
    ProductAdapter productAdapter;
    List<String> listDataHeader;
    HashMap<String,String> products_map;
    HashMap<String, List<String>> listDataChild;
    int position = 0;
    String cat_id="";
   // SubCategoryExpandableListAdapter subCategoryExpandableListAdapter;

    public SubcategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("title", "Search");
//        HomePage.titleTV.setText("Search Results");
        // Inflate the layout for this fragment
        Constant.hideKeyboard(getActivity());
        View view = inflater.inflate(R.layout.subcategory_fragment, container, false);
        Bundle b = getArguments();
        position = b.getInt("Position");
        cat_id = b.getString("cat_id");
        realm = RealmController.with(getActivity()).getRealm();
        initView(view);

        return view;
    }

    private void initView(View view) {


        products_map=new HashMap<>();
        productRV = (ListView) view.findViewById(R.id.productRV);
        productAdapter = new ProductAdapter(getActivity(), products_map);
        productRV.setAdapter(productAdapter);

        prepareListData();



    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {


            default:
                break;
        }


    }
    private void prepareListData() {

        DemoClass demoClass = realm.where(DemoClass.class).equalTo("id", 02)
                .findFirst();
        Log.d("resultss--))", "" + demoClass.getCategoryData());

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(demoClass.getCategoryData());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < jsonArray.length(); i++) {

            Log.d("Log_tag", "ii" + jsonArray);

            for (int j = 1; j == 1; j++) {
                JSONObject jsonObject = jsonArray.optJSONObject(position);

                JSONArray jsonArray1 = jsonObject.optJSONArray("subcategory");

                if (jsonArray1==null)
                {
                    Bundle bundle=new Bundle();
                    bundle.putString("cat_id",jsonObject.optString("cat_id"));
                    ProductFragment fragment = new ProductFragment();
                    fragment.setArguments(bundle);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .add(R.id.fragmentsContainer, fragment).addToBackStack(null)
                            .commit();
                    FragmentActivity.helper=true;
                    break;
                }
                else{
                    FragmentActivity.helper=false;
                    for (int p = 0; p < jsonArray1.length(); p++) {
                        JSONObject jsonObject1 = jsonArray1.optJSONObject(p);
                        products_map.put(jsonObject1.optString("subcategory_name"),jsonObject1.optString("subcategory_id"));
                       /* JSONArray jsonArray2 = jsonObject1.optJSONArray("innersubcategory");
                        List<String> first_list = new ArrayList<>();
                        for (int k = 0; k < jsonArray2.length(); k++) {

                            JSONObject jsonObject2 = jsonArray2.optJSONObject(k);
                            String value=jsonObject2.optString("mincategory_id")+":"+jsonObject2.optString("mincategory_name");
                            first_list.add(value);

                        }
                        Log.d("Log_tag", "p" + jsonArray1.length());
                        Log.d("Log_tag", "ppp" + jsonArray1);
                        listDataChild.put(listDataHeader.get(p), first_list);*/
                    }
                }


            }
            break;
        }

        productAdapter.addList(products_map);
        productAdapter.notifyDataSetChanged();
        productRV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d("onItemClick","--"+adapterView+"=="+view+"=="+i+"==");
                Bundle bundle=new Bundle();
                bundle.putString("cat_id",products_map.get((products_map.keySet().toArray())[i]));
                ProductFragment fragment = new ProductFragment();
                fragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragmentsContainer, fragment).addToBackStack("cd")
                        .commit();

            }
        });



    }
/*    private void prepareListData() {

        DemoClass demoClass = realm.where(DemoClass.class).equalTo("id", 02)
                .findFirst();
        Log.d("resultss--))", "" + demoClass.getCategoryData());

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(demoClass.getCategoryData());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < jsonArray.length(); i++) {

            Log.d("Log_tag", "ii" + jsonArray);


                JSONObject jsonObject = jsonArray.optJSONObject(i);
            Log.d("Log_tag", "ii" + jsonObject);



                 JSONArray jsonArray1 = jsonObject.optJSONArray("subcategory");

               if (jsonArray1==null)
                {
                    Bundle bundle=new Bundle();
                    bundle.putString("cat_id",jsonObject.optString("cat_id"));
                    ProductFragment fragment = new ProductFragment();
                    fragment.setArguments(bundle);
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragmentsContainer, fragment).addToBackStack(null)
                            .commit();
                   FragmentActivity.helper=true;
                    break;
                }
                else{
                    FragmentActivity.helper=false;
                    for (int p = 0; p < jsonArray1.length(); p++) {
                        JSONObject jsonObject1 = jsonArray1.optJSONObject(p);
                        listDataHeader.add(jsonObject1.optString("subcategory_name"));
                        JSONArray jsonArray2 = jsonObject1.optJSONArray("innersubcategory");
                        List<String> first_list = new ArrayList<>();
                        for (int k = 0; k < jsonArray2.length(); k++) {

                            JSONObject jsonObject2 = jsonArray2.optJSONObject(k);
                            String value=jsonObject2.optString("mincategory_id")+":"+jsonObject2.optString("mincategory_name");
                            first_list.add(value);

                        }
                        Log.d("Log_tag", "p" + jsonArray1.length());
                        Log.d("Log_tag", "ppp" + jsonArray1);
                        listDataChild.put(listDataHeader.get(p), first_list);
                    }
                }




        }
        Log.d("hashmap","---"+products_map.toString());
        productAdapter.addList(products_map);
        productAdapter.notifyDataSetChanged();
         //subCategoryExpandableListAdapter.notifyDataSetChanged();




    }*/

    @Override
    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
      //  Toast.makeText(getActivity(), listDataHeader.get(i) + " : " + listDataChild.get(listDataHeader.get(i)).get(i1), Toast.LENGTH_SHORT).show();
      //  Toast.makeText(getActivity(), view.toString(), Toast.LENGTH_SHORT).show();
        String[] value=listDataChild.get(listDataHeader.get(i)).get(i1).split(":");

        Bundle bundle=new Bundle();
        bundle.putString("cat_id",value[0]);
        ProductFragment fragment = new ProductFragment();
        fragment.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentsContainer, fragment).addToBackStack("cd")
                .commit();
        return false;
    }
}
