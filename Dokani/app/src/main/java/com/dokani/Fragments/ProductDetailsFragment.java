package com.dokani.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.dokani.Activity.FragmentActivity;
import com.dokani.Adapters.ImagePagerAdapter;
import com.dokani.Async_Thread.Super_AsyncTask;
import com.dokani.Async_Thread.Super_AsyncTask_Interface;
import com.dokani.Bean.DemoClass;
import com.dokani.Classes.AutoScrollViewPager;
import com.dokani.CommonUtils.Constant;
import com.dokani.MySharedPreferences;
import com.dokani.R;
import com.dokani.RealmDataBase.RealmController;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import io.realm.Realm;
import io.realm.RealmResults;
import me.relex.circleindicator.CircleIndicator;

public class ProductDetailsFragment extends Fragment implements View.OnClickListener {
    private TextView addToCartTV, addTowishTV, checkoutDetailTV, priceTV, prodDescriptionTV, prodNameTV;
    private CircleIndicator circleIndicatorCD;
    RatingBar ratingBarRB;
    private AutoScrollViewPager productDetailVP;
    private Realm realm;
    String price = "", name = "", image_url = "", rating = "", description = "",qty="";
    String product_id;

    public ProductDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("title", "Search");
//        HomePage.titleTV.setText("Search Results");
        // Inflate the layout for this fragment
        Constant.hideKeyboard(getActivity());
        View view = inflater.inflate(R.layout.product_detail, container, false);
        product_id = getArguments().getString("product_id");
        initView(view);
        getProductDetails(product_id);
        return view;
    }

    private void initView(View view) {
        realm = RealmController.with(getActivity()).getRealm();
        productDetailVP = (AutoScrollViewPager) view.findViewById(R.id.productDetailVP);
        circleIndicatorCD = (CircleIndicator) view.findViewById(R.id.circleIndicatorCD);
        addToCartTV = (TextView) view.findViewById(R.id.addToCartTV);
        addTowishTV = (TextView) view.findViewById(R.id.addTowishTV);
        checkoutDetailTV = (TextView) view.findViewById(R.id.checkoutDetailTV);

        priceTV = (TextView) view.findViewById(R.id.priceTV);
        prodDescriptionTV = (TextView) view.findViewById(R.id.prodDescriptionTV);
        prodNameTV = (TextView) view.findViewById(R.id.prodNameTV);
        ratingBarRB = (RatingBar) view.findViewById(R.id.ratingBarRB);
        ratingBarRB.setStepSize(0.1f);


        addToCartTV.setOnClickListener(this);
        addTowishTV.setOnClickListener(this);
        checkoutDetailTV.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.addToCartTV:
                RealmResults<DemoClass> results = realm.where(DemoClass.class).
                        contains("cartProductId", product_id + ":id").findAll();
                if (results.size() < 1) {
                    Log.d("qtyqty","--"+qty);
                    DemoClass demoClass = new DemoClass();
                    demoClass.setId(3 + System.currentTimeMillis());
                    demoClass.setCartProductName(name + ":name");
                    demoClass.setCartProductPrice(price + ":price");
                    demoClass.setCartProductUrl(image_url + "@url");
                    demoClass.setCartProductAvailabilityQty(qty + ":qty");
                    demoClass.setCartProductId(product_id + ":id");
                    demoClass.setCartProductQuantity("1" + ":qty");
                    demoClass.setCartProductRating(rating + ":rating");
                    Constant.copyDataToRealm(realm, demoClass);
                    Constant.initiatePopupWindow(getResources().getString(R.string.product_added_to_cart), getActivity());
                } else {


                    realm.beginTransaction();
                    String qty[] = results.get(0).getCartProductQuantity().split(":");
                    int quantity = Integer.parseInt(qty[0]) + 1;
                    results.get(0).setCartProductQuantity(String.valueOf(quantity));
                    realm.commitTransaction();
                    Constant.initiatePopupWindow(getResources().getString(R.string.product_qty_updated), getActivity());
                    Log.d("getData", "-id  present--");
                }
                break;
            case R.id.addTowishTV:
                RealmResults<DemoClass> results1 = realm.where(DemoClass.class).
                        contains("wishProductId", product_id + ":id").findAll();

                if (results1.size() < 1) {
                    DemoClass demoClass = new DemoClass();
                    demoClass.setId(4 + System.currentTimeMillis());
                    demoClass.setWishProductName(name + ":name");
                    demoClass.setWishProductPrice(price + ":price");
                    demoClass.setWishProductUrl(image_url + "@url");
                    demoClass.setWishProductId(product_id + ":id");
                    demoClass.setWishProductRating(rating + ":rating");
                    demoClass.setWishProductDesc(description + "@description");
                    Constant.copyDataToRealm(realm, demoClass);

                    Constant.initiatePopupWindow(getResources().getString(R.string.product_added_in_wish), getActivity());

                } else {
                    Constant.initiatePopupWindow(getResources().getString(R.string.product_already_in_wish), getActivity());
                    Log.d("getData", "-id  present--");
                }
                break;
            case R.id.checkoutDetailTV:
                String session = MySharedPreferences.getInstance().getData(getActivity(), "session_id");
                if (session.equals("null")) {
                    askForLogin();
                } else {
                    doTransactionOnCheckout();
                }

                break;


            default:
                break;
        }


    }

    private void askForLogin() {

        final String[] stringItems = {getResources().getString(R.string.login),getResources().getString(R.string.checkout_guest)};

        final ActionSheetDialog dialog = new ActionSheetDialog(getActivity(), stringItems, null,getResources().getString(R.string.cancel));
        dialog.isTitleShow(false).show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {


                if (position == 0) {
                    Log.e("pos1", "dalsjhdjk");
                    Intent login = new Intent(getActivity(), FragmentActivity.class);
                    login.putExtra("fragment", "LOGIN");
                    startActivity(login);

                } else if (position == 1) {
                    Log.e("pos2", "dalsjhdjk");

                    doTransactionOnCheckout();

                }
                dialog.dismiss();

            }
        });
    }


    private void doTransactionOnCheckout() {
        CheckOutFromSearch fragment = new CheckOutFromSearch();
        Bundle bundle1 = new Bundle();
        bundle1.putString("qty_size", "1");
        bundle1.putString("qty", "1");
        bundle1.putString("product_id", product_id);
        bundle1.putString("subtotal", price);
        fragment.setArguments(bundle1);
        FragmentActivity.fragActivitytitleTV.setText(getResources().getString(R.string.checkout));

        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentsContainer, fragment).addToBackStack(null)
                .commit();
    }

    private void setupViewPager(AutoScrollViewPager viewPager, CircleIndicator circleIndicator, ArrayList<String> banner_list,int layout) {

        ImagePagerAdapter imagePagerAdapter = new ImagePagerAdapter(getActivity(), banner_list,layout);
        viewPager.setAdapter(imagePagerAdapter);
        circleIndicator.setViewPager(viewPager);
        viewPager.startAutoScroll(2000);
        viewPager.setStopScrollWhenTouch(true);
        viewPager.setAutoScrollDurationFactor(17);


    }

    private void getProductDetails(String prod_id) {
        HashMap<String, String> map = new HashMap<>();
        Log.d("iddd", "prd_id" + prod_id);
        map.put("product_id", prod_id);
        if (Constant.haveNetworkConnection(getActivity())) {
            Constant.execute(new Super_AsyncTask(getActivity(), map, Constant.PRODUCT_DETAILS, new Super_AsyncTask_Interface() {

                @Override
                public void onTaskCompleted(String output) {
                    ArrayList<String> banner_list = new ArrayList<>();
                    Log.d("outputt---->>>", "PRODUCT_DETAILS" + output);
                    try {
                        JSONArray jsonArray = new JSONArray(output);
                        for (int i = 0; i < jsonArray.length(); i++) {


                            JSONObject jsonObject = jsonArray.optJSONObject(i);
                            if (jsonObject.optString("special_price").equals("")) {
                                price = jsonObject.optString("product_price");
                            } else {
                                price = jsonObject.optString("special_price");
                            }

                            description = jsonObject.optString("description");
                            name = jsonObject.optString("product_name");
                            String price1 = String.format("%.02f", Float.parseFloat(price));
                            priceTV.setText("OMR" + price1 + "");
                            rating = jsonObject.optString("product_rating");
                            image_url = jsonObject.optString("product_image_url");
                            qty = jsonObject.optString("qty");
                            prodDescriptionTV.setText(description + "");
                            prodNameTV.setText(name + "");
                            ratingBarRB.setRating(Float.parseFloat(rating));
                            JSONArray jsonArray1 = jsonObject.optJSONArray("gallery");
                            for (int k = 0; k < jsonArray1.length(); k++) {
                                banner_list.add(jsonArray1.optString(k));
                            }

                        }
                        setupViewPager(productDetailVP, circleIndicatorCD, banner_list,R.layout.pager_item2);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }, true, false));

        }
    }

}
