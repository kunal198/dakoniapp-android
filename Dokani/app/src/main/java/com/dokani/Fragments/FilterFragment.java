package com.dokani.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.dokani.Adapters.FilterExpandableListAdapter;
import com.dokani.Async_Thread.Super_AsyncTask;
import com.dokani.Async_Thread.Super_AsyncTask_Interface;
import com.dokani.CommonUtils.Constant;
import com.dokani.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class FilterFragment extends Fragment implements View.OnClickListener, ExpandableListView.OnChildClickListener {


    public static String price_value = "", color_value = "", manufactor_value = "",category_value;
    private ExpandableListView filterELV;
    private TextView applyTV;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    int position = 0;
    String cat_id = "";
    FilterExpandableListAdapter filterExpandableListAdapter;

    public FilterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("title", "Search");
//        HomePage.titleTV.setText("Search Results");
        // Inflate the layout for this fragment
        Constant.hideKeyboard(getActivity());
        View view = inflater.inflate(R.layout.filter_fragment, container, false);
        Bundle b = getArguments();

        cat_id = b.getString("cat_id");

        initView(view);

        return view;
    }

    private void initView(View view) {
        applyTV = (TextView) view.findViewById(R.id.applyTV);
        filterELV = (ExpandableListView) view.findViewById(R.id.filterELV);
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        filterExpandableListAdapter = new FilterExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
        filterELV.setAdapter(filterExpandableListAdapter);
        filterELV.setOnChildClickListener(this);
        applyTV.setOnClickListener(this);

        prepareDataList();
        filterELV.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return true; // This way the expander cannot be collapsed
            }
        });
    }

    private void prepareDataList() {
        HashMap hashMap = new HashMap();
        Log.d("idddddddd", "" + cat_id);
        hashMap.put("category_id", cat_id);

        if (Constant.haveNetworkConnection(getActivity())) {
            Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.FILTERABLE_LIST, new Super_AsyncTask_Interface() {

                @Override
                public void onTaskCompleted(String output) {
                    Log.d("FIlter_list", "" + output);
                    try {
                        JSONObject jsonObject2 = new JSONObject(output);
                        JSONObject jsonObject = jsonObject2.optJSONObject("data");
                        if (jsonObject!=null) {
                            Iterator iter = jsonObject.keys();
                            while (iter.hasNext()) {
                                String key = (String) iter.next();

                                Log.d("keyyyy","--"+key);
                                try {
                                    JSONArray price_array = jsonObject.optJSONArray(key);
                                    List<String> first_list = new ArrayList<>();
                                    if (price_array != null) {
                                        if (price_array.length() > 0) {
                                            for (int i = 0; i < price_array.length(); i++) {
                                                JSONObject jsonObject1 = price_array.optJSONObject(i);
                                                String value = jsonObject1.optString("label") + "@" + jsonObject1.optString("value");
                                                first_list.add(value);
                                            }
                                            listDataHeader.add(key);
                                            listDataChild.put(key, first_list);
                                        }
                                    }
                                } catch (Exception e) {
                                    // Something went wrong!}}
                                }
                            }
                           /* JSONArray price_array = jsonObject.optJSONArray("Price");


                            JSONArray manufacture_array = jsonObject.optJSONArray("Manufacturer");
                            if (manufacture_array != null) {


                                if (manufacture_array.length() > 0) {
                                    List<String> second_list = new ArrayList<>();
                                    for (int i = 0; i < manufacture_array.length(); i++) {
                                        JSONObject jsonObject1 = manufacture_array.optJSONObject(i);
                                        String value = jsonObject1.optString("label") + "@" + jsonObject1.optString("value");
                                        second_list.add(value);
                                    }
                                    listDataHeader.add("Manufacture");
                                    listDataChild.put("Manufacture", second_list);
                                }
                            }
                            JSONArray color_array = jsonObject.optJSONArray("Color");
                            if (color_array != null) {
                                if (color_array.length() > 0) {
                                    List<String> third_list = new ArrayList<>();
                                    for (int i = 0; i < color_array.length(); i++) {
                                        JSONObject jsonObject1 = color_array.optJSONObject(i);
                                        String value = jsonObject1.optString("label") + "@" + jsonObject1.optString("value");
                                        third_list.add(value);
                                    }
                                    listDataHeader.add("Color");
                                    listDataChild.put("Color", third_list);
                                }
                            }*/
                        }

                            filterExpandableListAdapter.notifyDataSetChanged();
                            for (int i = 0; i < filterExpandableListAdapter.getGroupCount(); i++) {
                                filterELV.expandGroup(i);
                            }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }, true, false));
        }

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.applyTV:
                Log.d("price", "---" + price_value + "=" + manufactor_value + "color" + color_value);
                HashMap hashMap = new HashMap();
                hashMap.put("category_id", category_value);
                hashMap.put("page_number", "1");
                hashMap.put("price", price_value);
                hashMap.put("manufacture", manufactor_value);
                hashMap.put("color", color_value);
                Log.d("hashmap", "" + hashMap.toString());
                if (Constant.haveNetworkConnection(getActivity())) {
                    Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.PRODUCTS_BY_FILTER, new Super_AsyncTask_Interface() {

                        @Override
                        public void onTaskCompleted(String output) {
                            price_value = "";
                            manufactor_value = "";
                            color_value = "";
                            category_value="";
                            Log.d("Filter_products-->", "Category_output" + output);
                            ProductFragment fragment = new ProductFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("output", output);
                            bundle.putString("cat_id", cat_id);
                            fragment.setArguments(bundle);
                            getActivity().getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragmentsContainer, fragment)
                                    .commit();


                        }

                    }, true, false));
                }
                break;

            default:
                break;
        }


    }


    @Override
    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {

        //  Toast.makeText(getActivity(), listDataHeader.get(i) + " : " + listDataChild.get(listDataHeader.get(i)).get(i1), Toast.LENGTH_SHORT).show();
        //  Toast.makeText(getActivity(), view.toString(), Toast.LENGTH_SHORT).show();
        String[] value=listDataChild.get(listDataHeader.get(i)).get(i1).split(":");

        Bundle bundle=new Bundle();
        bundle.putString("cat_id",value[0]);
        ProductFragment fragment = new ProductFragment();
        fragment.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentsContainer, fragment).addToBackStack(null)
                .commit();
        return false;
    }
}
