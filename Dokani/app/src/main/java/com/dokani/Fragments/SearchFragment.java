package com.dokani.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.RelativeLayout;

import com.dokani.Activity.FragmentActivity;
import com.dokani.Adapters.SearchListAdapter;
import com.dokani.Async_Thread.Super_AsyncTask;
import com.dokani.Async_Thread.Super_AsyncTask_Interface;
import com.dokani.Bean.HomePageBean;
import com.dokani.CommonUtils.Constant;
import com.dokani.R;
import com.dokani.RealmDataBase.RealmController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import io.realm.Realm;

public class SearchFragment extends Fragment implements View.OnClickListener {

    private ListView searchLV;
    private ImageView searchIV;
    private EditText searchET;
    private RelativeLayout noDataLayoutRL;
    private TextView foundProductsSizeTV;
    ArrayList<HomePageBean> data_list;
    Realm realm;

    SearchListAdapter searchListAdapter;
    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("title", "Search");
//        HomePage.titleTV.setText("Search Results");
        // Inflate the layout for this fragment
        Constant.hideKeyboard(getActivity());
        View view = inflater.inflate(R.layout.search_fragment_layout, container, false);

        initView(view);

        return view;
    }

    private void initView(View view) {
        searchLV = (ListView) view.findViewById(R.id.searchLV);
        searchET = (EditText) view.findViewById(R.id.searchET);
        searchIV = (ImageView) view.findViewById(R.id.searchIV);
        foundProductsSizeTV = (TextView) view.findViewById(R.id.foundProductsSizeTV);
        noDataLayoutRL= (RelativeLayout) view.findViewById(R.id.noDataLayoutRL);
        searchIV.setOnClickListener(this);
        searchET.setText("");
        realm= RealmController.with(getActivity()).getRealm();
        data_list=new ArrayList<>();
         searchListAdapter = new SearchListAdapter(getActivity(), data_list,realm);
        searchLV.setAdapter(searchListAdapter);
        searchLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent fragment_activityy = new Intent(getActivity(), FragmentActivity.class);
                fragment_activityy.putExtra("fragment", "PRODUCTS_DETAILS");
                fragment_activityy.putExtra("product_id",data_list.get(i).getProductId());
                startActivity(fragment_activityy);
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.searchIV:
                if (data_list!=null)
                {
                    data_list.clear();
                }
                HashMap hashMap = new HashMap();
                hashMap.put("search_string", searchET.getText().toString());
                hashMap.put("page_number", "1");
                Log.d("hashmap", "" + hashMap);
                if (Constant.haveNetworkConnection(getActivity())) {
                    Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.SEARCH_PRODUCTS, new Super_AsyncTask_Interface() {

                        @Override
                        public void onTaskCompleted(String output) {
                            Log.d("search_result", "---" + output);
                            try {
                                JSONArray jsonArray = new JSONArray(output);
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                                    JSONArray jsonArray1 = jsonObject.optJSONArray("products");
                                    if (jsonArray1.length() < 1) {
                                        searchLV.setVisibility(View.GONE);
                                        noDataLayoutRL.setVisibility(View.VISIBLE);
                                    } else {
                                        searchLV.setVisibility(View.VISIBLE);
                                        noDataLayoutRL.setVisibility(View.GONE);
                                        for (int k = 0; k < jsonArray1.length(); k++) {
                                            JSONObject jsonObject1 = jsonArray1.optJSONObject(k);
                                            HomePageBean homePageBean = new HomePageBean();
                                            homePageBean.setProductName(jsonObject1.optString("product_name"));

                                            if (jsonObject.optString("special_price").equals("")) {
                                                homePageBean.setProductPrice(jsonObject1.optString("product_price"));
                                            } else {
                                                homePageBean.setProductPrice(jsonObject1.optString("special_price"));
                                            }
                                            homePageBean.setProductSmallDesc(jsonObject1.optString("description"));
                                            homePageBean.setProductUrl(jsonObject1.optString("product_image_url"));
                                            homePageBean.setProductId(jsonObject1.optString("product_id"));
                                            homePageBean.setProductRating(jsonObject1.optString("product_rating"));
                                            data_list.add(homePageBean);

                                        }


                                    }

                                }
                                searchET.setText("");
                                searchListAdapter.notifyDataSetChanged();
                                foundProductsSizeTV.setText(data_list.size() + getResources().getString(R.string.prod_found));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }


                    }, true, false));
                }

                break;
            default:
                break;
        }


    }
}
