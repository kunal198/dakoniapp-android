package com.dokani.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.dokani.Activity.FragmentActivity;
import com.dokani.Activity.PaymentActivity;
import com.dokani.Adapters.CustomerAdapter;
import com.dokani.Async_Thread.Super_AsyncTask;
import com.dokani.Async_Thread.Super_AsyncTask_Interface;
import com.dokani.Bean.Country;
import com.dokani.CommonUtils.Constant;
import com.dokani.CommonUtils.CountryGetter;
import com.dokani.MySharedPreferences;
import com.dokani.R;
import com.dokani.RealmDataBase.RealmController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import io.realm.Realm;

/**
 * Created by brst-pc20 on 1/6/17.
 */

public class CheckOutFromSearch extends Fragment implements View.OnClickListener {
    TextView nextFromShippingTV, totalProductsTV, shippingChargesTV, subtotalTV, totalTV;
    AutoCompleteTextView countrySP, stateSP, citySP;
    EditText zipCodeET, addressET, phoneNoET, emailAddressET, lastNameET, firstNameET;
    CustomerAdapter stateAdpater, cityAdpater;
    ArrayList<Country> stateArrayList, cityArrayList;

    String product_id = "", qty_size = "", country_code = "", subtotal = "", quantities = "",from_cart="";
    private Realm realm;


    public CheckOutFromSearch() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.shipping_information, container, false);
        realm = RealmController.with(getActivity()).getRealm();
        nextFromShippingTV = (TextView) view.findViewById(R.id.nextFromShippingTV);
        shippingChargesTV = (TextView) view.findViewById(R.id.shippingChargesTV);
        subtotalTV = (TextView) view.findViewById(R.id.subtotalTV);
        totalTV = (TextView) view.findViewById(R.id.totalTV);
        zipCodeET = (EditText) view.findViewById(R.id.zipCodeET);
        addressET = (EditText) view.findViewById(R.id.addressET);
        phoneNoET = (EditText) view.findViewById(R.id.phoneNoET);
        emailAddressET = (EditText) view.findViewById(R.id.emailAddressET);
        lastNameET = (EditText) view.findViewById(R.id.lastNameET);
        firstNameET = (EditText) view.findViewById(R.id.firstNameET);

        totalProductsTV = (TextView) view.findViewById(R.id.totalProductsTV);
        countrySP = (AutoCompleteTextView) view.findViewById(R.id.countrySP);
        stateSP = (AutoCompleteTextView) view.findViewById(R.id.stateSP);
        citySP = (AutoCompleteTextView) view.findViewById(R.id.citySP);
        nextFromShippingTV.setOnClickListener(this);

        setSppinerCountry();
        getDataFromBundle();
        fillDataIfUserLogIn();
        return view;
    }

    private void getDataFromBundle() {
        Bundle bundle = getArguments();
        if (bundle != null) {

            product_id = bundle.getString("product_id");
            quantities = bundle.getString("qty");
            qty_size = bundle.getString("qty_size");
            subtotal = bundle.getString("subtotal");
            from_cart = bundle.getString("from_cart","no");
            totalProductsTV.setText(qty_size);
            subtotalTV.setText("OMR" + subtotal);


        }
    }

    private void setSppinerCountry() {
        ArrayList<Country> countryArrayList = new ArrayList<>();
        //  final TreeMap<String, String> map = new TreeMap<String, String>(countries);
        stateArrayList = new ArrayList<>();
        cityArrayList = new ArrayList<>();
        Country country = new Country();
        country.setCountryName("State");
        Country countryy = new Country();
        countryy.setCountryName("City");
        cityArrayList.add(countryy);
        stateArrayList.add(country);
        CountryGetter countryGetter = new CountryGetter(getActivity());

        countryArrayList = countryGetter.ReadFromfile();


        //SortSpinnerAdapter1 countryAdapter = new SortSpinnerAdapter1(getActivity(), R.layout.spinner_layout2, countryArrayList);

        // Drop down layout style - list view with radio button
        // countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        CustomerAdapter customerAdapter = new CustomerAdapter(getActivity(), R.layout.spinner_layout2, countryArrayList);

        countrySP.setThreshold(1);

        countrySP.setAdapter(customerAdapter);
        Log.d("countriesArrayList11", "--" + countryArrayList);


        final ArrayList<Country> finalCountryArrayList = countryArrayList;

        countrySP.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int i, long id) {
                String item = parent.getItemAtPosition(i).toString();

                String geoID = finalCountryArrayList.get(i).geoID();
                country_code = finalCountryArrayList.get(i).countryCode();
                stateArrayList.clear();
                Country country1 = new Country();
                country1.setCountryName("State");
                stateArrayList.add(country1);
                Log.d("resulttt", "--" + item
                        + "---" + geoID);
                if (!item.equals("Select Country")) {
                    setSppinerState(geoID, stateArrayList, stateAdpater);
                }
            }
        });

        stateAdpater = new CustomerAdapter(getActivity(), R.layout.spinner_layout3, stateArrayList);

        // Drop down layout style - list view with radio button
        //  stateAdpater.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        stateSP.setThreshold(1);
        stateSP.setAdapter(stateAdpater);


        stateSP.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int i, long id) {

                if (i != 0) {
                    String item = parent.getItemAtPosition(i).toString();
                    Log.d("statearrayList", "---" + stateArrayList);
                    String geoID = stateArrayList.get(i).geoID();
                    cityArrayList.clear();
                    Country country1 = new Country();
                    country1.setCountryName("City");
                    cityArrayList.add(country1);
                    Log.d("itemmm", "--" +
                            "---" + geoID);
                    setSppinerState(geoID, cityArrayList, cityAdpater);
                }
                //   country_code = map.get(item);

            }


        });

        cityAdpater = new CustomerAdapter(getActivity(), R.layout.spinner_layout3, cityArrayList);

        // Drop down layout style - list view with radio button
        // cityAdpater.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        citySP.setThreshold(1);
        citySP.setAdapter(cityAdpater);


        citySP.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int i, long id) {
                String item = parent.getItemAtPosition(i).toString();

                String geoID = cityArrayList.get(i).geoID();
                Log.d("itemmm", "--" + item
                        + "---" + geoID);
                //   country_code = map.get(item);

            }
        });
    }

    private void setSppinerState(String geoID, final ArrayList<Country> arrayList, final CustomerAdapter sortSpinnerAdapter1) {

        Log.d("countriesArrayList11", "--" + stateArrayList);

        String url = Constant.GET_CITIES + geoID + Constant.GEONAME_USERNAME;
        Log.d("urllll", "--" + url);
        if (Constant.haveNetworkConnection(getActivity())) {
            Constant.execute(new Super_AsyncTask(getActivity(), url, new Super_AsyncTask_Interface() {
                @Override
                public void onTaskCompleted(String result) {
                    Log.d("states_op", "" + result);
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        JSONArray jsonArray = jsonObject.optJSONArray("geonames");
                        if (jsonArray != null) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                Country country = new Country();
                                JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                country.setCountryName(jsonObject1.optString("name"));
                                String geonameId = jsonObject1.optString("geonameId");
                                Log.d("geonameId>>>>", "--" + geonameId);
                                country.setGeoNameId(jsonObject1.optString("geonameId"));
                                arrayList.add(country);
                            }

                        }
                        sortSpinnerAdapter1.addList(arrayList);
                        sortSpinnerAdapter1.notifyDataSetChanged();




                      /*  stateAdpater = new CustomerAdapter(getActivity(), R.layout.spinner_layout3, arrayList);

                        // Drop down layout style - list view with radio button
                        //  stateAdpater.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


                        stateSP.setThreshold(1);
                        stateSP.setAdapter(stateAdpater);*/
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, true, false));


        }

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            /*RealmResults<DemoClass> results = realm.where(DemoClass.class).
                    contains("cartProductId","id")
                    .findAll();*/
            case R.id.nextFromShippingTV:

                if (shippingChargesTV.getText().toString().isEmpty() || shippingChargesTV.getText().toString().equals("-")) {
                    if (validationOfData()) {

                        final HashMap hashMap = new HashMap();


                        if (country_code.isEmpty()) {
                            country_code = countrySP.getText().toString();
                        }


                        hashMap.put("product_id", product_id);
                        hashMap.put("product_qty", quantities);
                        hashMap.put("country_id", country_code);
                        hashMap.put("post_code", zipCodeET.getText().toString());


                        Log.d("hashhhhhhh", "--" + hashMap);
                        if (Constant.haveNetworkConnection(getActivity())) {
                            HashMap hashMap1 = new HashMap();
                            hashMap1.put("productsids", hashMap.get("product_id"));
                            hashMap1.put("product_qty", hashMap.get("product_qty"));
                            Log.d("hashmap", "==" + hashMap1.toString());
                            Constant.execute(new Super_AsyncTask(getActivity(), hashMap1, Constant.CHECK_QTY, new Super_AsyncTask_Interface() {
                                @Override
                                public void onTaskCompleted(String result) {
                                    Log.d("check_qtyy", "--" + result);

                                    JSONObject jsonObject = null;
                                    try {
                                        jsonObject = new JSONObject(result);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    if (jsonObject.optString("success").equals("false")) {
                                        new AlertDialog.Builder(getActivity())
                                                .setTitle(getResources().getString(R.string.alert))
                                                .setMessage(jsonObject.optString("message"))
                                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        ((FragmentActivity) getActivity()).onBackPressed();
                                                    }
                                                })
                                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // do nothing
                                                    }
                                                })
                                                .setIcon(R.mipmap.ic_alert)
                                                .show();
                                    } else {


                                        if (Constant.haveNetworkConnection(getActivity())) {
                                            Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.SHIPPING_CHARGES, new Super_AsyncTask_Interface() {
                                                @Override
                                                public void onTaskCompleted(String result) {
                                                    Log.d("ship_result", "--" + result);
                                                    try {
                                                        JSONArray jsonArray1 = new JSONArray(result);
                                                        JSONObject jsonObject = jsonArray1.optJSONObject(0);
                                                        if (jsonObject.optString("success").equals("true")) {
                                                            String shipping_price = jsonObject.optString("Price");
                                                            double total_price = Double.parseDouble(subtotal) + Double.parseDouble(shipping_price);
                                                            totalTV.setText("OMR" + total_price);
                                                            shippingChargesTV.setText("OMR" + shipping_price);
                                                            nextFromShippingTV.setText("Submit");
                                                        }

                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }

                                                }
                                            }, true, false));
                                        }
                                    }
                                }
                            }, true, false));
                        }
                    }
                } else {


                    HashMap hashMap = new HashMap();
                    hashMap.put("email", emailAddressET.getText().toString());
                    hashMap.put("productsids", product_id);
                    hashMap.put("product_qty", quantities);
                    hashMap.put("firstname", firstNameET.getText().toString());
                    hashMap.put("lastname", lastNameET.getText().toString());
                    hashMap.put("zipcode", zipCodeET.getText().toString());
                    hashMap.put("pay_method", "checkmo");
                    hashMap.put("address", addressET.getText().toString());
                    hashMap.put("city", citySP.getText().toString());
                    hashMap.put("country_id", country_code);
                    hashMap.put("region", stateSP.getText().toString());
                    hashMap.put("telephone", phoneNoET.getText().toString());
                    Intent intent = new Intent(getActivity(), PaymentActivity.class);
                    intent.putExtra("ammount", totalTV.getText().toString());
                    intent.putExtra("all_data", hashMap);
                    intent.putExtra("from_cart", from_cart);

                    startActivity(intent);
                    // getActivity().finish();
                }
                break;
        }

    }

    private boolean validationOfData() {
        boolean status = true;
        if (!Constant.validateFields(firstNameET.getText().toString())) {
            firstNameET.setError(getResources().getString(R.string.please_enter_first_name));
            status = false;
        }
        if (!Constant.validateFields(lastNameET.getText().toString())) {
            lastNameET.setError(getResources().getString(R.string.please_enter_last_name));
            status = false;
        }

        if (!Constant.emailValidator(emailAddressET.getText().toString())) {
            emailAddressET.setError(getResources().getString(R.string.please_enter_email));
            status = false;
        }
        if (!Constant.validateFields(phoneNoET.getText().toString())) {
            phoneNoET.setError(getResources().getString(R.string.please_enter_phone_number));
            status = false;
        }
        if (!Constant.validateFields(addressET.getText().toString())) {
            addressET.setError(getResources().getString(R.string.please_enter_address));
            status = false;
        }
        if (!Constant.validateFields(countrySP.getText().toString())) {
            countrySP.setError(getResources().getString(R.string.please_enter_country));
            status = false;
        }
        if (!Constant.validateFields(stateSP.getText().toString())) {
            stateSP.setError(getResources().getString(R.string.please_choose_state));
            status = false;
        }
        if (!Constant.validateFields(citySP.getText().toString())) {
            citySP.setError(getResources().getString(R.string.please_choose_city));
            status = false;
        }
        if (!Constant.validateFields(zipCodeET.getText().toString())) {
            zipCodeET.setError(getResources().getString(R.string.please_enter_zip));
            status = false;
        }
        return status;
    }

    private void fillDataIfUserLogIn() {
        if (!MySharedPreferences.getInstance().getData(getActivity(), "user_data").equals("null")) {

            Log.d("account", "dataentered");
            String data = MySharedPreferences.getInstance().getData(getActivity(), "user_data");

            try {
                JSONObject jsonObject = new JSONObject(data);

                firstNameET.setText(jsonObject.optString("firstname"));
                emailAddressET.setText(jsonObject.optString("email"));
                phoneNoET.setText(jsonObject.optString("telephone"));

                addressET.setText(jsonObject.optString("street"));
                zipCodeET.setText(jsonObject.optString("postcode"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
