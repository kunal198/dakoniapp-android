package com.dokani.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dokani.Activity.FragmentActivity;
import com.dokani.Activity.HomePage;
import com.dokani.Async_Thread.Super_AsyncTask;
import com.dokani.Async_Thread.Super_AsyncTask_Interface;
import com.dokani.CommonUtils.Constant;
import com.dokani.MySharedPreferences;
import com.dokani.R;
import com.dokani.RealmDataBase.RealmController;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import io.realm.Realm;

/**
 * Created by brst-pc20 on 1/6/17.
 */

public class LoginFragment extends Fragment implements View.OnClickListener {
    String fb_url="";
    String name,last_name,email="",id;
    TextView signUpTV, forgotPassTV, loginTV, userNameET, passwordET,loginWithFbTV;
    private Realm realm;
    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.login, container, false);
        initView(view);


        return view;
    }

    private void initView(View view)
    {
        passwordET = (TextView) view.findViewById(R.id.passwordET);
        userNameET = (TextView) view.findViewById(R.id.userNameET);
        loginTV = (TextView) view.findViewById(R.id.loginTV);
        forgotPassTV = (TextView) view.findViewById(R.id.forgotPassTV);
        signUpTV = (TextView) view.findViewById(R.id.signUpTV);
        loginWithFbTV = (TextView) view.findViewById(R.id.loginWithFbTV);
        loginWithFbTV.setOnClickListener(this);
        signUpTV.setOnClickListener(this);
        forgotPassTV.setOnClickListener(this);
        loginTV.setOnClickListener(this);

        this.realm = RealmController.with(this).getRealm();
        initFb();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.signUpTV:
                FragmentActivity.fragActivitytitleTV.setText(getResources().getString(R.string.signup));
                SignUpFragment signUpFragment = new SignUpFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragmentsContainer, signUpFragment)
                        .commit();
                break;
            case R.id.forgotPassTV:
                FragmentActivity.fragActivitytitleTV.setText(getResources().getString(R.string.forgot_pass));
                ForgotPassword forgotPassword = new ForgotPassword();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragmentsContainer, forgotPassword).addToBackStack(null)
                        .commit();
                break;
            case R.id.loginTV:

                String token=MySharedPreferences.getInstance().getData(getActivity(),"firebase_taken");

                HashMap<String, String> hashMap = new HashMap();
                hashMap.put("email", userNameET.getText().toString());
                hashMap.put("password", passwordET.getText().toString());
                hashMap.put("devicetoken", token);
                hashMap.put("platform", "android");
                Log.d("mappppppppp", "" + hashMap);
                if (Constant.haveNetworkConnection(getActivity())) {
                    Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.LOGIN_API, new Super_AsyncTask_Interface() {

                        @Override
                        public void onTaskCompleted(String output) {
                            Log.d("login_op", "" + output);

                            try {
                                JSONObject jsonObject = new JSONObject(output);
                                if (jsonObject.optString("success").equals("true")) {


                                    // getActivity().finish();
                                    MySharedPreferences.getInstance().storeData(getActivity(), "session_id", jsonObject.optString("session_id"));
                                    MySharedPreferences.getInstance().storeData(getActivity(), "customer_id", jsonObject.optString("customerid"));
                                    MySharedPreferences.getInstance().storeData(getActivity(), "user_data", output);

                                    getActivity().finish();
                                    // HomePage.viewpager.setCurrentItem(0);
                                } else {
                                    Constant.showAlert2(LoginFragment.this, getResources().getString(R.string.alert), jsonObject.optString("message"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }, true, false));

                }
                break;
            case R.id.loginWithFbTV:
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email"));
                break;
        }

    }
   /* public void requestUserProfile(LoginResult loginResult){
        GraphRequest.newMeRequest(
                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject me, GraphResponse response) {

                        Log.d("jsonnnn","---"+me);
                        String email = null;
                        try {
                            email = response.getJSONObject().get("email").toString();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.e("Resultttttt","--"+ email);
                        if (response.getError() != null) {
                            // handle error
                        } else {
                            try {
                                String emaill = response.getJSONObject().get("email").toString();
                                Log.e("Resulttt","--"+ email);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String id = me.optString("id");
                            // send email and id to your web server
                            Log.e("Result1", response.getRawResponse());
                            Log.e("Result", me.toString());
                        }
                    }
                }).executeAsync();
    }
*/
private void initFb()
{
    FacebookSdk.sdkInitialize(getActivity());
    FacebookSdk.setIsDebugEnabled(true);
   // FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
    FragmentActivity.mCallbackManager = CallbackManager.Factory.create();
    Log.d("callbackkk","--"+FragmentActivity.mCallbackManager);
    LoginManager.getInstance().registerCallback( FragmentActivity.mCallbackManager,
            new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {

                    Log.d("Successssss", "Login"+loginResult.toString());
                    //requestUserProfile(loginResult);
                                AccessToken accessToken = loginResult.getAccessToken();
                                Profile profile = Profile.getCurrentProfile();

                                // Facebook Email address
                                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                            @Override
                                            public void onCompleted(JSONObject object, GraphResponse response) {
                                                Log.d("LoginActivity_Response ", object+"");

                                                try {
                                                    if (object.has("first_name"))
                                                    {

                                                        name = object.getString("first_name");
                                                    }
                                                    if (object.has("last_name"))
                                                    {
                                                        last_name = object.getString("last_name");
                                                    }
                                                    if (object.has("email"))
                                                    {
                                                        email = object.getString("email");
                                                    }
                                                    JSONObject jsonObject=object.optJSONObject("picture");
                                                    JSONObject jsonObject1=jsonObject.optJSONObject("data");
                                                    fb_url=jsonObject1.optString("url");
                                                    MySharedPreferences.getInstance().storeData(getActivity(),"social_url",jsonObject1.optString("url"));




                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                                String token=MySharedPreferences.getInstance().getData(getActivity(),"firebase_taken");

                                                HashMap hashMap=new HashMap();
                                                hashMap.put("email",email);
                                                hashMap.put("firstname",name);
                                                hashMap.put("lastname",last_name);
                                                hashMap.put("devicetoken",token);
                                                hashMap.put("platform", "android");
                                                hashMap.put("profile", fb_url);
                                                Log.d("hashmap","---"+hashMap);
                                                if (Constant.haveNetworkConnection(getActivity())) {
                                                    Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.SOCIAL_LOGIN, new Super_AsyncTask_Interface() {

                                                        @Override
                                                        public void onTaskCompleted(String output) {
                                                            Log.d("login_op", "" + output);

                                                            try {
                                                                JSONObject jsonObject = new JSONObject(output);
                                                                if (jsonObject.optString("success").equals("true")) {
                                                                    Intent home_page = new Intent(getActivity(), HomePage.class);
                                                                    startActivity(home_page);
                                                                    getActivity().finish();
                                                                    MySharedPreferences.getInstance().storeData(getActivity(), "session_id", jsonObject.optString("session_id"));
                                                                    MySharedPreferences.getInstance().storeData(getActivity(), "user_data", output);

                                                                    // HomePage.viewpager.setCurrentItem(0);
                                                                } else {
                                                                    Constant.showAlert2(LoginFragment.this, getResources().getString(R.string.alert), jsonObject.optString("message"));
                                                                }
                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }

                                                        }

                                                    }, true, false));
                                                }
                                            }
                                        });

                                Bundle parameters = new Bundle();
                                parameters.putString("fields", "id,first_name,last_name,email,picture.type(large)");
                                request.setParameters(parameters);
                                request.executeAsync();
                }

                @Override
                public void onCancel() {
                    Toast.makeText(getActivity(), "Login Cancel", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onError(FacebookException exception) {
                    Toast.makeText(getActivity(), exception.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
}


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(FragmentActivity.mCallbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }
    }
}


