package com.dokani.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dokani.Activity.HomePage;
import com.dokani.CommonUtils.Constant;
import com.dokani.R;

/**
 * Created by brst-pc20 on 1/6/17.
 */

public class AboutUsFragment extends Fragment{
    public AboutUsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.about_us, container, false);





        return view;
    }




}
