package com.dokani.Fragments;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.TextViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dokani.CommonUtils.Constant;
import com.dokani.R;

import java.util.List;
import java.util.Stack;

/**
 * Created by brst-pc20 on 1/6/17.
 */

public class ContactUsFragment extends Fragment implements View.OnClickListener{

    TextView phoneNoTV,emailTV;
    public ContactUsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.contact_us, container, false);
        initView(view);




        return view;
    }


    private void initView(View view)
    {
        phoneNoTV= (TextView) view.findViewById(R.id.phoneNoTV);
        emailTV= (TextView) view.findViewById(R.id.emailTV);
        phoneNoTV.setOnClickListener(this);
        emailTV.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.phoneNoTV:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:1234567890"));
               // intent.setData(Uri.parse(phoneNoTV.getText().toString()));
                startActivity(intent);
                break;

            case R.id.emailTV:

                String[] TO = {emailTV.getText().toString()};

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("*/*");

                i.putExtra(Intent.EXTRA_EMAIL, TO);
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");


                startActivity(createEmailOnlyChooserIntent(i, "Send via email"));

                break;
        }
    }
    public Intent createEmailOnlyChooserIntent(Intent source,
                                               CharSequence chooserTitle) {
        Stack<Intent> intents = new Stack<Intent>();
        Intent i = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto",
                "info@domain.com", null));
        List<ResolveInfo> activities = getActivity().getPackageManager()
                .queryIntentActivities(i, 0);

        for (ResolveInfo ri : activities) {
            Intent target = new Intent(source);
            target.setPackage(ri.activityInfo.packageName);
            intents.add(target);
        }

        if (!intents.isEmpty()) {
            Intent chooserIntent = Intent.createChooser(intents.remove(0),
                    chooserTitle);
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                    intents.toArray(new Parcelable[intents.size()]));

            return chooserIntent;
        } else {
            return Intent.createChooser(source, chooserTitle);
        }
    }
}
