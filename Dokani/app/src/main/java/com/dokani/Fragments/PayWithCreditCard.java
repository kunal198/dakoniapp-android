package com.dokani.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.dokani.Activity.PaymentActivity;
import com.dokani.Async_Thread.Super_AsyncTask;
import com.dokani.Async_Thread.Super_AsyncTask_Interface;
import com.dokani.Bean.DemoClass;
import com.dokani.CommonUtils.Constant;
import com.dokani.R;
import com.dokani.RealmDataBase.RealmController;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.realm.Realm;
import io.realm.RealmResults;


public class PayWithCreditCard extends Fragment implements View.OnClickListener {

    private TextView checkoutTV, paymentAmmountTV, phoneNoET, postalCodeTV;

    private Stripe stripe;
    private String ammount = "", from_cart = "";
    HashMap hashMap;
    // private String PUBLISHABLE_KEY ="pk_test_C8TZ044NbjwxCw8pAFRdVjtP";
    private String PUBLISHABLE_KEY = "pk_test_IhQpOt7bBw1TdKNCayJ58Tw4";
    Card card;
    private EditText expiryDateTV, cardNumberTV, cardNameTV, expiryDateYearTV, securityCode;
    private Realm realm;
    ProgressDialog dialog;

    public PayWithCreditCard() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.pay_via_credit_card, container, false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            ammount = bundle.getString("ammount");
            Log.d("mapppppppp", "--" + hashMap);
            hashMap = (HashMap) bundle.getSerializable("all_data");
            Log.d("ammount--", "--" + ammount);
            ammount = ammount.replace("OMR", "");
            from_cart = bundle.getString("from_cart");
        }
        initView(view);


        return view;
    }

    private void initView(View view) {

        realm = RealmController.with(getActivity()).getRealm();
        checkoutTV = (TextView) view.findViewById(R.id.checkoutTV);
        paymentAmmountTV = (TextView) view.findViewById(R.id.paymentAmmountTV);
        cardNumberTV = (EditText) view.findViewById(R.id.cardNumberTV);
        cardNameTV = (EditText) view.findViewById(R.id.cardNameTV);
        expiryDateTV = (EditText) view.findViewById(R.id.expiryDateTV);
        securityCode = (EditText) view.findViewById(R.id.securityCode);
        expiryDateYearTV = (EditText) view.findViewById(R.id.expiryDateYearTV);
        phoneNoET = (TextView) view.findViewById(R.id.phoneNoET);
        postalCodeTV = (TextView) view.findViewById(R.id.postalCodeTV);
        paymentAmmountTV.setText(ammount);
        phoneNoET.setText(hashMap.get("telephone").toString());
        postalCodeTV.setText(hashMap.get("zipcode").toString());

        ((PaymentActivity) getActivity()).checkoutFrame.setVisibility(View.GONE);
        ((PaymentActivity) getActivity()).paymentFrame.setVisibility(View.VISIBLE);

        checkoutTV.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.checkoutTV:
                if (validateFields(cardNumberTV,getResources().getString(R.string.please_enter_card_no))&&validateFields(expiryDateTV,getResources().getString(R.string.please_enter_expiry_date))&&validateFields(expiryDateYearTV,getResources().getString(R.string.please_enter_expiry_year))&&validateFields(securityCode,getResources().getString(R.string.please_enter_security_code)))
                {
                    buy();
                }

                break;
        }
    }


    private boolean validateFields(EditText editText,String error)
    {
        if (editText.getText().toString().isEmpty())
        {
            editText.setError(error);
            return  false;
        }
        return  true;
    }


    private void buy() {
        Constant.hideKeyboard(getActivity());
        dialog = ProgressDialog.show(getActivity(), "", "");
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.progress_dialog);
        dialog.show();
        int date_month = Integer.parseInt(expiryDateTV.getText().toString());
        int date_year = Integer.parseInt(expiryDateYearTV.getText().toString());
        card = new Card(cardNumberTV.getText().toString(), date_month, date_year, securityCode.getText().toString());
        boolean validation = card.validateCard();
        if (validation) {

            new Stripe(getActivity()).createToken(card, PUBLISHABLE_KEY,
                    new TokenCallback() {
                        @Override
                        public void onError(Exception error) {
                            Log.d("Stripe", error.toString());
                            Constant.showAlert2(getActivity(), getResources().getString(R.string.alert), error.toString());

                            dialog.dismiss();
                        }

                        @Override
                        public void onSuccess(Token token) {

                            HashMap hashMap = new HashMap();
                            Log.d("tokennnn", "--=" + token.getId());
                            hashMap.put("stripeToken", token.getId());
                            hashMap.put("amount", ammount);
                            Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.STRIPE, new Super_AsyncTask_Interface() {
                                @Override
                                public void onTaskCompleted(String result) {

                                    dialog.dismiss();
                                    Log.d("result", "==" + result);
                                    try {
                                        JSONObject jsonObject = new JSONObject(result);
                                        if (jsonObject.optString("success").equals("true")) {
                                            if (from_cart.equals("yes")) {
                                                RealmResults<DemoClass> resultss = realm.where(DemoClass.class).
                                                        contains("cartProductName", "name")
                                                        .or()
                                                        .contains("cartProductUrl", "url")
                                                        .or()
                                                        .contains("cartProductPrice", "price")
                                                        .or()
                                                        .contains("cartProductId", "id")
                                                        .or()
                                                        .contains("cartProductRating", "rating")
                                                        .or()
                                                        .contains("cartProductQuantity", "qty")
                                                        .findAll();
                                                realm.beginTransaction();
                                                resultss.clear();
                                                realm.commitTransaction();
                                            }

                                            Constant.showAlert3(PayWithCreditCard.this, getResources().getString(R.string.alert), jsonObject.optString("message"));
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, false, false));
                        }
                    });
        } else if (!card.validateNumber()) {
            dialog.dismiss();
            Constant.showAlert2(getActivity(), getResources().getString(R.string.alert), getResources().getString(R.string.invalid_card_no));
            Log.d("Stripe", "The card number that you entered is invalid");
        } else if (!card.validateExpiryDate()) {
            dialog.dismiss();
            Constant.showAlert2(getActivity(), getResources().getString(R.string.alert), getResources().getString(R.string.invalid_expiry_date));
            Log.d("Stripe", "The expiration date that you entered is invalid");
        } else if (!card.validateCVC()) {
            dialog.dismiss();
            Constant.showAlert2(getActivity(), getResources().getString(R.string.alert),getResources().getString(R.string.invalid_cvc));
            Log.d("Stripe", "The CVC code that you entered is invalid");
        } else {
            dialog.dismiss();
            Constant.showAlert2(getActivity(), getResources().getString(R.string.alert), getResources().getString(R.string.invalid_card_details));
            Log.d("Stripe", "The card details that you entered are invalid");
        }
    }


}
