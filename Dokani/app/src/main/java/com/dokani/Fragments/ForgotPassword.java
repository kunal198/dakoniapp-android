package com.dokani.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.dokani.Activity.FragmentActivity;
import com.dokani.Async_Thread.Super_AsyncTask;
import com.dokani.Async_Thread.Super_AsyncTask_Interface;
import com.dokani.CommonUtils.Constant;
import com.dokani.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ForgotPassword extends Fragment implements View.OnClickListener {

    private EditText emailIdET;
    private TextView sendEmailTV;
    public ForgotPassword() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        View view = inflater.inflate(R.layout.forgot_password, container, false);
        initView(view);



        return view;
        // Inflate the layout for this fragment

    }

    private void initView(View view) {
        emailIdET= (EditText) view.findViewById(R.id.emailIdET);
        sendEmailTV= (TextView) view.findViewById(R.id.sendEmailTV);
        sendEmailTV.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.sendEmailTV:
                HashMap<String, String> hashMap = new HashMap();
                hashMap.put("email", emailIdET.getText().toString());
                if (Constant.haveNetworkConnection(getActivity())) {
                    Constant.execute(new Super_AsyncTask(getActivity(), hashMap, Constant.FORGOT_PASSWORD, new Super_AsyncTask_Interface() {

                        @Override
                        public void onTaskCompleted(String output) {
                            Log.d("login_op", "" + output);

                            try {
                                JSONObject jsonObject = new JSONObject(output);
                                if (jsonObject.optString("success").equals("true")) {
                                    FragmentActivity.fragActivitytitleTV.setText(getResources().getString(R.string.login));
                                    LoginFragment loginFragment = new LoginFragment();
                                    getActivity().getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.fragmentsContainer, loginFragment)
                                            .commit();
                                    Constant.showAlert2(ForgotPassword.this, getResources().getString(R.string.alert), jsonObject.optString("message"));
                                } else {
                                    Constant.showAlert2(ForgotPassword.this, getResources().getString(R.string.alert), jsonObject.optString("message"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }, true, false));
                }
                break;
        }

    }
}
