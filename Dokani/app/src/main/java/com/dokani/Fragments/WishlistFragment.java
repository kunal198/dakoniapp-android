package com.dokani.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.dokani.Activity.FragmentActivity;
import com.dokani.Adapters.WishListAdapter;
import com.dokani.Bean.DemoClass;
import com.dokani.CommonUtils.Constant;
import com.dokani.R;
import com.dokani.RealmDataBase.RealmController;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;


public class WishlistFragment extends Fragment implements SwipeMenuListView.OnMenuItemClickListener, View.OnClickListener {
    ArrayList<DemoClass> results2;
    private SwipeMenuListView wishListLV;
    private Realm realm;
    WishListAdapter wishListAdapter;
    public LinearLayout wishListDataLayoutLL;
    public RelativeLayout wishnoDataLayoutRL;
    private EditText searchET;
    private ImageView searchIV;

    public WishlistFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d("title", "wishlist");
        Constant.hideKeyboard(getActivity());

        realm = RealmController.with(getActivity()).getRealm();
        View view = inflater.inflate(R.layout.wishlist_fragment_view, container, false);
        wishListLV = (SwipeMenuListView) view.findViewById(R.id.wishListLV);
        wishListDataLayoutLL = (LinearLayout) view.findViewById(R.id.wishListDataLayoutLL);
        searchET = (EditText) view.findViewById(R.id.searchET);
        searchIV = (ImageView) view.findViewById(R.id.searchIV);
        wishnoDataLayoutRL = (RelativeLayout) view.findViewById(R.id.wishnoDataLayoutRL);
        searchIV.setOnClickListener(this);
        searchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                // if (i == EditorInfo.IME_ACTION_GO) {
                // do your stuff here
                if (searchET.getText().toString().isEmpty()) {
                    RealmResults<DemoClass> results = realm.where(DemoClass.class).
                            contains("wishProductName", "name")
                            .or()
                            .contains("wishProductUrl", "url")
                            .or()
                            .contains("wishProductPrice", "price")
                            .or()
                            .contains("wishProductId", "id")
                            .or()
                            .contains("wishProductRating", "rating")
                            .or()
                            .contains("wishProductDesc", "description")
                            .findAll();
                    ArrayList<DemoClass> results_list = new ArrayList<DemoClass>();
                    results_list.addAll(results);
                    wishListAdapter.addList(results_list);
                    wishListAdapter.notifyDataSetChanged();
                }
                Constant.hideKeyboard(getActivity());
                //     }
                return false;
            }
        });
        SwipeMenuCreator swipeMenuCreator = Constant.swipeCreator();
        setAdapterOnwishListScreen(swipeMenuCreator);
        // Inflate the layout for this fragment
        return view;
    }

    private void setAdapterOnwishListScreen(SwipeMenuCreator swipeMenuCreator) {
        final RealmResults<DemoClass> results = realm.where(DemoClass.class).
                contains("wishProductName", "name")
                .or()
                .contains("wishProductUrl", "url")
                .or()
                .contains("wishProductPrice", "price")
                .or()
                .contains("wishProductId", "id")
                .or()
                .contains("wishProductRating", "rating")
                .or()
                .contains("wishProductDesc", "description")
                .findAll();
        if (results.size() == 0) {
            wishnoDataLayoutRL.setVisibility(View.VISIBLE);
            wishListDataLayoutLL.setVisibility(View.GONE);

        } else {
            ArrayList<DemoClass> resultss = new ArrayList<>();
            resultss.addAll(results);
            wishListDataLayoutLL.setVisibility(View.VISIBLE);
            wishnoDataLayoutRL.setVisibility(View.GONE);
            wishListAdapter = new WishListAdapter(getActivity(), resultss,realm,WishlistFragment.this);
            wishListLV.setAdapter(wishListAdapter);
            wishListLV.setMenuCreator(swipeMenuCreator);
            wishListLV.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

            wishListLV.setOnMenuItemClickListener(this);
        }
        wishListLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent fragment_activityy = new Intent(getActivity(), FragmentActivity.class);
                fragment_activityy.putExtra("fragment", "PRODUCTS_DETAILS");
                fragment_activityy.putExtra("product_id",results.get(i).getWishProductId());
                startActivity(fragment_activityy);
            }
        });


    }


    @Override
    public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
        switch (index) {

            case 0:
                Log.d("index", "--" + index);
              RealmResults<DemoClass> results1=  fetchDataWishList();
                realm.beginTransaction();

                // remove single match
                results1.remove(position);
                realm.commitTransaction();
                ArrayList<DemoClass> datalist=new ArrayList<>();
                datalist.addAll(fetchDataWishList());
                wishListAdapter.addList(datalist);
                wishListAdapter.notifyDataSetChanged();

                if (results1.size() == 0) {
                    wishnoDataLayoutRL.setVisibility(View.VISIBLE);
                    wishListDataLayoutLL.setVisibility(View.GONE);
                }
                break;

        }
        return false;
    }


    private  RealmResults<DemoClass>  fetchDataWishList()
    {
        RealmResults<DemoClass> results1 = realm.where(DemoClass.class).
                contains("wishProductName", "name")
                .or()
                .contains("wishProductUrl", "url")
                .or()
                .contains("wishProductPrice", "price")
                .or()
                .contains("wishProductId", "id")
                .or()
                .contains("wishProductRating", "rating")
                .findAll();
        return results1;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.searchIV:
                //  wishListAdapter.clearList();
                ArrayList<DemoClass> results2 = new ArrayList<>();
                RealmResults<DemoClass> results1 = realm.where(DemoClass.class).
                        contains("wishProductName", "name")
                        .or()
                        .contains("wishProductUrl", "url")
                        .or()
                        .contains("wishProductPrice", "price")
                        .or()
                        .contains("wishProductId", "id")
                        .or()
                        .contains("wishProductRating", "rating")
                        .findAll();

                for (int i = 0; i < results1.size(); i++) {
                    Log.d("filter_list", "--" + results1.size());
                    if (results1.get(i).getWishProductName().toString().toLowerCase().contains(searchET.getText().toString().toLowerCase())) {

                        Log.d("filter_list", "--enter");
                        results2.add(results1.get(i));

                    }

                }
                Log.d("filter_list", "--" + results2.toString());
                wishListAdapter.addList(results2);
                wishListAdapter.notifyDataSetChanged();
                break;
        }
    }
}
