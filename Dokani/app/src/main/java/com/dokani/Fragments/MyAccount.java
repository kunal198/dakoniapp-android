package com.dokani.Fragments;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.dokani.Adapters.SortSpinnerAdapter;
import com.dokani.CommonUtils.Constant;
import com.dokani.MySharedPreferences;
import com.dokani.R;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;


public class MyAccount extends Fragment implements View.OnTouchListener, View.OnClickListener {
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 1001;
    private TextView editprofileTV, nameEditET, updateprofileTV, emailET, phoneET, sexET, ageET, addressET, stateET, cityET, zipET;
    private ImageView cameraIconIV, edit_profileIV;
    private CircularImageView circularprofileIV;
    private String picturePath= "";
    private File destination;
    private Spinner sexSP;
    static String encoded_base64="";
    int GALLERY_CODE = 201;
    InputStream profile_pic_data;
    private ProgressDialog progress;
    boolean isFragmentVisible=false;
    public MyAccount() {
        // Required empty public constructor
    }

    View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment

        if (view == null) {
            view = inflater.inflate(R.layout.edit_profile, container, false);
            initView(view);
        }


        return view;
    }


    private void initView(View view) {
        editprofileTV = (TextView) view.findViewById(R.id.editprofileTV);
        updateprofileTV = (TextView) view.findViewById(R.id.updateprofileTV);
        edit_profileIV = (ImageView) view.findViewById(R.id.edit_profileIV);
        nameEditET = (TextView) view.findViewById(R.id.nameEditET);
        emailET = (TextView) view.findViewById(R.id.emailET);
        phoneET = (TextView) view.findViewById(R.id.phoneET);
        sexSP = (android.widget.Spinner) view.findViewById(R.id.sexSP);
        ageET = (TextView) view.findViewById(R.id.ageET);
        addressET = (TextView) view.findViewById(R.id.addressET);
        stateET = (TextView) view.findViewById(R.id.stateET);
        cityET = (TextView) view.findViewById(R.id.cityET);
        zipET = (TextView) view.findViewById(R.id.zipET);
        setSppiner();

        cameraIconIV = (ImageView) view.findViewById(R.id.cameraIconIV);
        circularprofileIV = (CircularImageView) view.findViewById(R.id.circularprofileIV);

     //   cameraIconIV.setOnClickListener(this);
        editprofileTV.setOnTouchListener(this);
        edit_profileIV.setOnClickListener(this);
        updateprofileTV.setOnClickListener(this);
        //getFacebookProfilePicture();


    }


    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        Log.d("setMenuVisibility","setMenuVisibility"+menuVisible);
        if (menuVisible)
        {
            isFragmentVisible=menuVisible;
            // cartAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isFragmentVisible)
        {

            settingDataOnWall();
            isFragmentVisible=false;
        }
    }

    private void settingDataOnWall() {
        Log.d("stored_data", "" + MySharedPreferences.getInstance().getData(getActivity(), "user_data"));
        if (!MySharedPreferences.getInstance().getData(getActivity(), "user_data").equals("null")) {

            Log.d("account", "dataentered");
            String data = MySharedPreferences.getInstance().getData(getActivity(), "user_data");


            Log.d(" ", "data----" + data);
            try {
                JSONObject jsonObject = new JSONObject(data);
                nameEditET.setText(jsonObject.optString("firstname"));
                emailET.setText(jsonObject.optString("email"));
                phoneET.setText(jsonObject.optString("telephone"));
                if (jsonObject.optString("gender").equals("1"))
                    sexSP.setSelection(0);
                else
                    sexSP.setSelection(1);
                ageET.setText(jsonObject.optString("dob"));
                addressET.setText(jsonObject.optString("street"));
                stateET.setText(jsonObject.optString("region"));
                cityET.setText(jsonObject.optString("city"));
                String imageData=jsonObject.optString("profile");
                zipET.setText(jsonObject.optString("postcode"));

                Log.d("account", "data----" + data);

                if (!imageData.isEmpty())
                {
                    Picasso.with(getActivity()).load(imageData).into(circularprofileIV);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            Log.d("account", "entered");
            // nameEditET.setEnabled(true);
            Picasso.with(getActivity()).load(R.mipmap.ic_banner_placeholder).into(circularprofileIV);
            nameEditET.setText("");
            emailET.setText("");
            phoneET.setText("");
            sexSP.setSelection(0);
            ageET.setText("");
            addressET.setText("");
            stateET.setText("");
            cityET.setText("");
            zipET.setText("");
        }
    }


    private void setSppiner() {
        ArrayList<String> categories = new ArrayList<String>();

        categories.add(getResources().getString(R.string.male));
        categories.add(getResources().getString(R.string.female));

        // Creating adapter for spinner
        SortSpinnerAdapter dataAdapter = new SortSpinnerAdapter(getActivity(), R.layout.spinner_layout4, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        sexSP.setAdapter(dataAdapter);
        sexSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String item = adapterView.getItemAtPosition(i).toString();
                Log.d("itemmm", "--" + item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        final int DRAWABLE_LEFT = 0;
        final int DRAWABLE_TOP = 1;
        final int DRAWABLE_RIGHT = 2;
        final int DRAWABLE_BOTTOM = 3;

        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            if (motionEvent.getRawX() >= (editprofileTV.getRight() - editprofileTV.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                // your action here
                Log.d("nameedit", "nameedit");
                //nameEditTV.setEnabled(true);
                return true;
            }
        }

        return false;
    }

    @Override
    public void onClick(View view) {


        switch (view.getId()) {
            case R.id.cameraIconIV:
              getYourBackground();

                break;
            case R.id.edit_profileIV:
               if (!MySharedPreferences.getInstance().getData(getActivity(), "session_id").equals("null"))
               {
                   cameraIconIV.setOnClickListener(this);
                   nameEditET.setEnabled(true);
                  // emailET.setEnabled(true);
                   phoneET.setEnabled(true);
                   sexSP.setEnabled(true);
                   ageET.setEnabled(true);
                   addressET.setEnabled(true);
                   stateET.setEnabled(true);
                   cityET.setEnabled(true);
                   zipET.setEnabled(true);
                   updateprofileTV.setEnabled(true);
                   updateprofileTV.setVisibility(View.VISIBLE);
               }
                else{
                   Constant.showAlert(getActivity(),getResources().getString(R.string.alert),getResources().getString(R.string.pls_do_login));
               }


                break;
            case R.id.updateprofileTV:

                cameraIconIV.setOnClickListener(null);

                nameEditET.setEnabled(false);
                emailET.setEnabled(false);
                phoneET.setEnabled(false);
                sexSP.setEnabled(false);
                ageET.setEnabled(false);
                addressET.setEnabled(false);
                stateET.setEnabled(false);
                cityET.setEnabled(false);
                zipET.setEnabled(false);
                updateprofileTV.setEnabled(false);

             /*   if (Constant.validateFields(nameEditET.getText().toString())&&Constant.emailValidator(emailET.getText().toString()))
                {
                    updateFields();
                }
                else {
                    Constant.showAlert2(getActivity(),getResources().getString(R.string.alert),"");
                }*/
                updateFields();
                break;
        }

    }

    private void updateFields() {
        final String cust_id = MySharedPreferences.getInstance().getData(getActivity(), "customer_id");
        int gender = 0;
        if (sexSP.getSelectedItem().equals("Male")) {
            gender = 0;
        } else if (sexSP.getSelectedItem().equals("Female")) {
            gender = 1;
        }
        final HashMap hashMap = new HashMap();
        hashMap.put("firstname", nameEditET.getText().toString());
        hashMap.put("email", emailET.getText().toString());
        hashMap.put("customer_id", cust_id);
        hashMap.put("mobile", phoneET.getText().toString());
        hashMap.put("gender", String.valueOf(gender));
        hashMap.put("age", ageET.getText().toString());
        hashMap.put("street", addressET.getText().toString());
        hashMap.put("region", stateET.getText().toString());
        hashMap.put("city", cityET.getText().toString());
        hashMap.put("postcode", zipET.getText().toString());



        if (Constant.haveNetworkConnection(getActivity())) {


             new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progress = ProgressDialog.show(getActivity(), "", "");
                progress.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                progress.setContentView(R.layout.progress_dialog);
                progress.show();


            }



                 @Override
                 protected String doInBackground(Void... voids) {
                     return Constant.upload(hashMap,profile_pic_data);
                 }

                 @Override
                 protected void onPostExecute(String s) {
                     super.onPostExecute(s);

                     progress.dismiss();
                     try {
                         JSONObject jsonObject=new JSONObject(s);
                         if (jsonObject.optString("success").equals("true"))
                         {
                             MySharedPreferences.getInstance().storeData(getActivity(),"user_data",s);
                             Constant.showAlert2(getActivity(),getResources().getString(R.string.alert),jsonObject.optString("message"));
                         }
                         else{

                             Constant.showAlert2(getActivity(),getResources().getString(R.string.alert),jsonObject.optString("message"));
                         }
                     } catch (JSONException e) {
                         e.printStackTrace();
                     }
                 }
             }.execute();

        }

      /*  OkHttpClient client = new OkHttpClient();

        MediaType mediaType = MediaType.parse("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
        RequestBody body = RequestBody.create(mediaType, "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"firstname\"\r\n\r\nafe\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"email\"\r\n\r\nas@gmail.com\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"customer_id\"\r\n\r\n54\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"mobile\"\r\n\r\n686486\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"gender\"\r\n\r\n1\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"age\"\r\n\r\n65\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"street\"\r\n\r\ndefw\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"region\"\r\n\r\nwef\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"city\"\r\n\r\nwef\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"postcode\"\r\n\r\n889\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"profile\"; filename=\"bag.jpg\"\r\nContent-Type: image/jpeg\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--");
        Request request = new Request.Builder()
                .url("http:///%7B%7BUrl%7D%7Deditprofile")
                .post(body)
                .addHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
                .addHeader("cache-control", "no-cache")
                .build();

        try {
            Response response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

    }


    public void getFacebookProfilePicture() {

        if (!MySharedPreferences.getInstance().getData(getActivity(), "social_url").equals("null")) {
           String user_Pic= MySharedPreferences.getInstance().getData(getActivity(), "social_url");
            Picasso.with(getActivity()).load(user_Pic).placeholder(R.mipmap.ic_banner_placeholder).into(circularprofileIV);

            Log.d("urlll","nonnull");
        }


    }

    private void captureImage() {

        picturePath = "";
        destination = null;
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        picturePath = String.valueOf(new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg"));

        destination = new File(picturePath);

        Log.e("FileDestination", "" + destination);
        Log.e("picturePath", "" + picturePath);


        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("onActivityResult","requestCode"+requestCode+"data"+data+"resultCode"+resultCode);
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE&&resultCode==getActivity().RESULT_OK) {



            BitmapFactory.Options options;
            options = new BitmapFactory.Options();
            options.inSampleSize = 2;

            final Bitmap bitmap = BitmapFactory.decodeFile(
                    destination.getAbsolutePath(), options);

            circularprofileIV.setImageBitmap(bitmap);
            backGroundThread(bitmap);


        }
        if (requestCode == GALLERY_CODE ) {
            try {

                Log.d("imagee","--"+GALLERY_CODE);
                picturePath = "";
                Uri selectedImage = data.getData();

                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                Log.d("gallery_path", "" + picturePath);
                cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                BitmapFactory.Options options;
                options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);

                circularprofileIV.setImageBitmap(bitmap);
                backGroundThread(bitmap);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }




    private void getYourBackground() {

        final String[] stringItems = {getResources().getString(R.string.choose),getResources().getString(R.string.take_photo),getResources().getString(R.string.choose_gallery)};

        final ActionSheetDialog dialog = new ActionSheetDialog(getActivity(), stringItems, null,getResources().getString(R.string.cancel));
        dialog.isTitleShow(false).show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {


                if (position == 1) {
                    Log.e("pos1", "dalsjhdjk");
                    captureImage();

                } else if (position == 2) {
                    Log.e("pos2", "dalsjhdjk");

                    try {
                        Intent intent = new Intent();
// Show only images, no videos or anything else
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
// Always show the chooser (if there are multiple options available)
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_CODE);
                      /*  Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, GALLERY_CODE);*/
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }


                }
                dialog.dismiss();

            }
        });
    }

   private  void backGroundThread(final Bitmap bitmap)
    {
        new AsyncTask<Void, Void, Void>() {


            @Override
            protected Void doInBackground(Void... params) {
            /*bmp.createScaledBitmap(bmp, 736, 414, true);*/
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 60, bos);
                 profile_pic_data = new ByteArrayInputStream(bos.toByteArray());







                return null;
            }


        }.execute();
    }
}
