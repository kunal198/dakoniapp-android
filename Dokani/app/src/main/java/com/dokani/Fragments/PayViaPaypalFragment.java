package com.dokani.Fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dokani.Activity.HomePage;
import com.dokani.Async_Thread.Super_AsyncTask;
import com.dokani.Async_Thread.Super_AsyncTask_Interface;
import com.dokani.Bean.DemoClass;
import com.dokani.CommonUtils.Constant;
import com.dokani.R;
import com.dokani.RealmDataBase.RealmController;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;
import java.util.HashMap;

import io.realm.Realm;
import io.realm.RealmResults;


public class PayViaPaypalFragment extends Fragment implements View.OnClickListener {

    private TextView loginToPaypalTV, nameEditTV;
    private String ammount="",from_cart="";
    PayPalConfiguration config;
    HashMap hashMap;
    private Realm realm;

    public PayViaPaypalFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Constant.hideKeyboard(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.pay_via_paypal, container, false);

        realm= RealmController.with(getActivity()).getRealm();
        Bundle bundle=getArguments();
        if (bundle!=null) {
            ammount = bundle.getString("ammount");

            hashMap= (HashMap) bundle.getSerializable("all_data");
            from_cart= bundle.getString("from_cart");
            Log.d("mapppppppp","--"+hashMap);
            Log.d("ammount--","--"+ammount);
            ammount=ammount.replace("OMR","");

        }

        initView(view);


        return view;
    }

    private void initView(View view) {
        ((com.dokani.Activity.PaymentActivity)getActivity()).checkoutFrame.setVisibility(View.GONE);
        ((com.dokani.Activity.PaymentActivity)getActivity()).paymentFrame.setVisibility(View.VISIBLE);

        loginToPaypalTV= (TextView) view.findViewById(R.id.loginToPaypalTV);
        loginToPaypalTV.setOnClickListener(this);

         config = new PayPalConfiguration()

                // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
                // or live (ENVIRONMENT_PRODUCTION)
                .environment(PayPalConfiguration.ENVIRONMENT_NO_NETWORK)


                .clientId(PayPalConfiguration.ENVIRONMENT_SANDBOX);

            Intent intent = new Intent(getActivity(), PayPalService.class);

            intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

            getActivity().startService(intent);


    }
    @Override
    public void onDestroy() {
        getActivity().stopService(new Intent(getActivity(), PayPalService.class));
        super.onDestroy();
    }
    private void getPayment() {
        //Getting the amount from editText

        //Creating a paypalpayment
        PayPalPayment payment = new PayPalPayment(new BigDecimal(ammount), "USD", "Simplified Coding Fee",
                PayPalPayment.PAYMENT_INTENT_SALE);

        //Creating Paypal Payment activity intent
        Intent intent = new Intent(getActivity(), PaymentActivity.class);

        //putting the paypal configuration to the intent
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);

        //Puting paypal payment to the intent
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

        //Starting the intent activity for result
        //the request code will be used on the method onActivityResult
        startActivityForResult(intent, 100);
    }


    @Override
    public void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    Log.i("paymentExample", confirm.toJSONObject().toString(4));
                    Log.d("hashhhhamap","="+hashMap);

                    if (Constant.haveNetworkConnection(getActivity())) {
                       // hashMap.put("pay_method","");
                    Constant.execute(new Super_AsyncTask(getActivity(),hashMap, Constant.PAYMENT_API, new Super_AsyncTask_Interface() {
                        @Override
                        public void onTaskCompleted(String result) {
                            Log.d("hashhhhamap_result","="+result);

                            if (from_cart.equals("yes"))
                            {
                                RealmResults<DemoClass> resultss = realm.where(DemoClass.class).
                                        contains("cartProductName","name")
                                        .or()
                                        .contains("cartProductUrl","url")
                                        .or()
                                        .contains("cartProductPrice","price")
                                        .or()
                                        .contains("cartProductId","id")
                                        .or()
                                        .contains("cartProductRating","rating")
                                        .or()
                                        .contains("cartProductQuantity","qty")
                                        .findAll();
                                realm.beginTransaction();
                                resultss.clear();
                                realm.commitTransaction();
                            }


                            new AlertDialog.Builder(getActivity())
                                    .setTitle(getResources().getString(R.string.alert))
                                    .setMessage("Your order is placed Successfully")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // continue with delete
                                            Intent intent = new Intent(getActivity(), HomePage.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK );

                                            startActivity(intent);

                                            getActivity().finish();


                                        }
                                    })

                                    .setIcon(R.mipmap.ic_alert)
                                    .setCancelable(true)
                                    .show();



                        }
                    },true,false));}
                    // TODO: send 'confirm' to your server for verification.
                    // see https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                    // for more details.

                } catch (JSONException e) {
                    Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                }
            }
        }
        else if (resultCode == Activity.RESULT_CANCELED) {
            Constant.showAlert2(getActivity(),getResources().getString(R.string.alert),"Payment canceled");
            Log.i("paymentExample", "The user canceled.");
        }
        else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {

            Constant.showAlert2(getActivity(),getResources().getString(R.string.alert),"Invalid data");
            Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
        }
    }

    @Override
    public void onClick(View view) {

        switch(view.getId())
        {
           case R.id.loginToPaypalTV:

               getPayment();

            break;
        }


    }
}
