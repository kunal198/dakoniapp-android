package com.dokani.PopUp;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dokani.Async_Thread.Super_AsyncTask;
import com.dokani.Async_Thread.Super_AsyncTask_Interface;
import com.dokani.CommonUtils.Constant;
import com.dokani.MySharedPreferences;
import com.dokani.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


/**
 * Created by brst-pc20 on 8/20/16.
 */
public class RatingPopUp extends Dialog implements View.OnClickListener {

    private static Context mactivity;
    private TextView closeTV, submitTV;
    private RatingBar ratingBarRB;
    private ImageView productIV;
    private String rating_str = "2", product_id = "",url="";

    public RatingPopUp(Context activity, String productid,String url) {
        super(activity);
        mactivity = activity;
        this.product_id = productid;
        this.url = url;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.rate_our_product);
        ratingBarRB = (RatingBar) findViewById(R.id.ratingBarRB);
        productIV = (ImageView) findViewById(R.id.productIV);
        Log.d("urlll","--"+url);

        Glide.with(mactivity).load(url).into(productIV);
        ratingBarRB.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {

                rating_str = String.valueOf(rating);

            }
        });
        closeTV = (TextView) findViewById(R.id.closeTV);
        submitTV = (TextView) findViewById(R.id.submitTV);
        submitTV.setOnClickListener(this);
        closeTV.setOnClickListener(this);
        Log.d("hello", "popupenter");
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.closeTV:
                dismiss();
                break;
            case R.id.submitTV:
                if (!MySharedPreferences.getInstance().getData(mactivity, "user_data").equals("null")) {

                    Log.d("account", "dataentered");
                    String data = MySharedPreferences.getInstance().getData(mactivity, "user_data");

                    try {
                        JSONObject jsonObject = new JSONObject(data);


                        HashMap hashMap = new HashMap();
                        hashMap.put("customer_id", jsonObject.optString("customerid"));
                        hashMap.put("rating", rating_str);
                        hashMap.put("product_id", product_id);
                        Log.d("hashmappp", "--" + hashMap);
                        Constant.execute(new Super_AsyncTask(mactivity, hashMap, Constant.RATING_API, new Super_AsyncTask_Interface() {
                            @Override
                            public void onTaskCompleted(String result) {
                                Log.d("rating_results", "--" + result);

                                try {
                                    JSONObject jsonObject1 = new JSONObject(result);
                                    if (jsonObject1.optString("success").equals("true")) {

                                        alert(jsonObject1.optString("message"));
                                    } else {

                                        alert(jsonObject1.optString("message"));
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, true, false));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Constant.showAlert2(mactivity,mactivity.getResources().getString(R.string.alert),mactivity.getResources().getString(R.string.pls_do_login));
                    dismiss();
                }

                break;
            default:
                break;
        }


    }

    private void alert(String msg) {
        new AlertDialog.Builder(mactivity)
                .setTitle(mactivity.getResources().getString(R.string.alert))
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

                    dismiss();
                    }
                })
                /*.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })*/
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}