package com.dokani;

import android.app.Application;

/**
 * Created by brst-pc93 on 1/24/17.
 */

public class MyApplication  extends Application
{

    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }


}