package com.dokani.Activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.dokani.CommonUtils.Constant;
import com.dokani.Fragments.AboutUsFragment;
import com.dokani.Fragments.CheckOutFromSearch;
import com.dokani.Fragments.ContactUsFragment;
import com.dokani.Fragments.LoginFragment;
import com.dokani.Fragments.ProductDetailsFragment;
import com.dokani.Fragments.SubcategoryFragment;
import com.dokani.MySharedPreferences;
import com.dokani.R;
import com.facebook.CallbackManager;

import java.util.Locale;


/**
 * Created by brst-pc20 on 1/5/17.
 */

public class FragmentActivity extends AppCompatActivity {
    public static boolean helper=false;
    public static TextView fragActivitytitleTV, fragActivitybackTV;
    int pos=0;
    String product_id="",subtotal="",qty="",qty_size="";
    public static  CallbackManager mCallbackManager;
    int from_cart=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container);
        initView();
        String fragment = getIntent().getStringExtra("fragment");
        Log.d("fragments","--"+fragment);
        if (fragment.equals("CHECK_OUT_SEARCH"))
        {
          
           // Log.d("product_ids","--"+getIntent().getStringArrayExtra("product_id_arr"));
            product_id= getIntent().getStringExtra("product_id");
            subtotal=getIntent().getStringExtra("subtotal");
            qty=getIntent().getStringExtra("qty");
            qty_size=getIntent().getStringExtra("qty_size");
            from_cart=getIntent().getIntExtra("from_cart",0);
//            Log.d("quantity>>>>","---"+qty[0]+"arr"+product_id+"arr"+"---"+product_id);
        }
        if (fragment.equals("PRODUCTS_DETAILS"))
        {
            product_id= getIntent().getStringExtra("product_id");
        }
        pos=getIntent().getIntExtra("Position",0);
      
        if (savedInstanceState == null) {
             FragmentTransaction ft= getSupportFragmentManager().beginTransaction();
                   ft .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                   ft .replace(R.id.fragmentsContainer, getFragmentReference(fragment)).addToBackStack(null);
                   ft .commit();
         /// overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
        }


    }

    public void changeLanguage(View view)
    {
        String languageToLoad  = "ar";
        String data= MySharedPreferences.getInstance().getData(FragmentActivity.this,"current_lang");
        Log.d("dataaaa","=="+data);
        if (MySharedPreferences.getInstance().getData(FragmentActivity.this,"current_lang").equals("null") ||MySharedPreferences.getInstance().getData(FragmentActivity.this,"current_lang").equals("english"))
        {
            languageToLoad  = "ar";
            MySharedPreferences.getInstance().storeData(FragmentActivity.this,"current_lang","arabic");
        }
        else {


            languageToLoad  = "en_US";
            MySharedPreferences.getInstance().storeData(FragmentActivity.this,"current_lang","english");
        }


        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        restartActivity();
    }
    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constant.hideKeyboard(FragmentActivity.this);
        Log.d("countt","---"+getFragmentManager().getBackStackEntryCount());
        int entry_count=getSupportFragmentManager().getBackStackEntryCount();
        if (entry_count==0|| helper)
            finish();
    }

    private void initView() {
        fragActivitybackTV = (TextView) findViewById(R.id.fragActivitybackTV);
        fragActivitytitleTV = (TextView) findViewById(R.id.fragActivitytitleTV);
    }


    private Fragment getFragmentReference(String key) {
        Fragment fragment = null;

        switch (key) {

            case "LOGIN":
                fragActivitytitleTV.setText(getResources().getString(R.string.login));
                fragment = new LoginFragment();

                break;
            case "CONTACT_US":
                fragActivitytitleTV.setText(getResources().getString(R.string.contact_us));
                fragment = new ContactUsFragment();

                break;
            case "ABOUT_US":
                fragActivitytitleTV.setText(getResources().getString(R.string.about_us));
                fragment = new AboutUsFragment();

                break;
            case "CHECK_OUT_SEARCH":
                fragActivitytitleTV.setText(getResources().getString(R.string.checkout));
                fragment = new CheckOutFromSearch();
                Bundle bundle1=new Bundle();
                Log.d("product_id","--"+product_id);
                bundle1.putString("qty",qty);
                bundle1.putString("subtotal",subtotal);
                bundle1.putString("qty_size",qty_size);
                bundle1.putString("product_id",product_id);
                if (from_cart==1)
                    bundle1.putString("from_cart","yes");
                else
                    bundle1.putString("from_cart","no");

                fragment.setArguments(bundle1);

                break;
            case "SUBCATEGORY":
                fragActivitytitleTV.setText(getResources().getString(R.string.product));
                fragment = new SubcategoryFragment();
                Bundle bundle=new Bundle();
                bundle.putInt("Position",pos);
                bundle.putString("cat_id","214");
                fragment.setArguments(bundle);

                break;
            case "PRODUCTS_DETAILS":
                fragActivitytitleTV.setText(getResources().getString(R.string.product_detail));
                fragment = new ProductDetailsFragment();
                Bundle bundl=new Bundle();
                bundl.putInt("Position",pos);
                bundl.putString("product_id",product_id);
                fragment.setArguments(bundl);

                break;

        }

        return fragment;
    }

    public void goBack(View view) {
        Constant.hideKeyboard(FragmentActivity.this);
       onBackPressed();

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        FragmentActivity.mCallbackManager.onActivityResult(requestCode, resultCode, data);
        if(mCallbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }
    }
}
