package com.dokani.Activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dokani.Adapters.DrawerList_Adapter;
import com.dokani.Classes.NonSwipeableViewPager;
import com.dokani.CommonUtils.Constant;
import com.dokani.FragmentDrawer;
import com.dokani.Fragments.CartFragment;
import com.dokani.Fragments.HomePageFragment;
import com.dokani.Fragments.MyAccount;
import com.dokani.Fragments.SearchFragment;
import com.dokani.Fragments.WishlistFragment;
import com.dokani.MySharedPreferences;
import com.dokani.R;
import com.dokani.RealmDataBase.RealmController;
import com.dokani.Service.DeleteTokenService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;

/**
 * Created by brst-pc20 on 1/2/17.
 */


public class HomePage extends AppCompatActivity implements ViewPager.OnPageChangeListener {
//Navigation Drawer Start

    boolean isUserLoggedIn = false;
    Toolbar mToolbar;
    DrawerLayout mDrawerLayout;
    FragmentDrawer drawerFragment;
    ListView listv_drawer;
    ArrayList<String> listDataHeader;
    ArrayList<Integer> listImageHeader;
    ArrayList<Integer> listImageHeaderSelected;

    DrawerList_Adapter drawer_adapter;
    Context context;
    Realm realm;
    Fragment fragment;
    ImageView logoIV;
//Navigation Drawer End


    public static NonSwipeableViewPager viewpager;
    private TabLayout tabLayout;
    public static TextView titleTV;

    private int[] tabIcons = {R.mipmap.ic_home, R.mipmap.ic_search, R.mipmap.ic_wishlist, R.mipmap.ic_cart, R.mipmap.ic_account};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_page);
        initViews();
        context = this;


        setUpIds();
        prepareListData();


    }

    public void changeLanguage(View view)
    {
        String languageToLoad  = "ar";
        String data=MySharedPreferences.getInstance().getData(HomePage.this,"current_lang");
        Log.d("dataaaa","=="+data);
        if (MySharedPreferences.getInstance().getData(HomePage.this,"current_lang").equals("null") ||MySharedPreferences.getInstance().getData(HomePage.this,"current_lang").equals("english"))
        {
            languageToLoad  = "ar";
            MySharedPreferences.getInstance().storeData(HomePage.this,"current_lang","arabic");
        }
        else {


            languageToLoad  = "en_US";
            MySharedPreferences.getInstance().storeData(HomePage.this,"current_lang","english");
        }


        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        restartActivity();
    }


    private void setUpIds() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        Constant.hideKeyboard(HomePage.this);

        listv_drawer = (ListView) findViewById(R.id.listv_drawer);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

       // logoIV= (ImageView) findViewById(R.id.logoIV);

        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);

      /*  logoIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });*/
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listImageHeader = new ArrayList<>();
        listImageHeaderSelected = new ArrayList<>();
        drawer_adapter = new DrawerList_Adapter(context, listDataHeader, listImageHeader, listImageHeaderSelected, 0);
        listv_drawer.setAdapter(drawer_adapter);
        makeArrayList();


        listv_drawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                displayView(position);
                Constant.hideKeyboard(HomePage.this);
                drawer_adapter.changeSelectedBackground(position);
                drawer_adapter.notifyDataSetChanged();
            }
        });

        displayView(0);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        makeArrayList();
    }
    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
    public void makeArrayList() {
        if (MySharedPreferences.getInstance().getData(HomePage.this, "session_id").equals("null")) {
            isUserLoggedIn = false;
        } else {
            isUserLoggedIn = true;

        }
        listImageHeaderSelected.clear();
        listImageHeader.clear();
        listDataHeader.clear();


        listDataHeader.add(getResources().getString(R.string.home));
        listDataHeader.add(getResources().getString(R.string.search));
        listDataHeader.add(getResources().getString(R.string.change_address));
        listDataHeader.add(getResources().getString(R.string.my_cart));
        listDataHeader.add(getResources().getString(R.string.about_us));
        listDataHeader.add(getResources().getString(R.string.contact_us));
        listDataHeader.add(getResources().getString(R.string.share_app));
        if (!isUserLoggedIn) {
            listDataHeader.add(getResources().getString(R.string.login));
        } else {
            listDataHeader.add(getResources().getString(R.string.logout));

        }




        listImageHeader.add(R.mipmap.ic_homee);
        listImageHeader.add(R.mipmap.ic_drawer_search);


        listImageHeader.add(R.mipmap.ic_drawer_edit);

        listImageHeader.add(R.mipmap.ic_drawer_mycart);
        listImageHeader.add(R.mipmap.ic_drawer_about);
        listImageHeader.add(R.mipmap.ic_drawer_contact);
        listImageHeader.add(R.mipmap.ic_share_white);
        if (!isUserLoggedIn) {
            listImageHeader.add(R.mipmap.ic_drawer_myaccount);
        } else {
            listImageHeader.add(R.mipmap.ic_drawer_logout);
        }

        listImageHeaderSelected.add(R.mipmap.ic_home_black);
        listImageHeaderSelected.add(R.mipmap.ic_search_black);


        listImageHeaderSelected.add(R.mipmap.ic_edit_black);
        listImageHeaderSelected.add(R.mipmap.ic_cart_black);
        listImageHeaderSelected.add(R.mipmap.ic_about_black);
        listImageHeaderSelected.add(R.mipmap.ic_contact_black);
        listImageHeaderSelected.add(R.mipmap.ic_share_black);
        if (!isUserLoggedIn) {
            listImageHeaderSelected.add(R.mipmap.ic_myacc_black);
        } else {
            listImageHeaderSelected.add(R.mipmap.ic_logout_black);
        }
        drawer_adapter.notifyDataSetChanged();
    }

    public void displayView(int groupPosition) {
        int position = 0;

        switch (groupPosition) {


            case 0:
                position = 0;
                break;


            case 1:
                position = 1;
                break;

            case 7:
                if (Constant.haveNetworkConnection(HomePage.this)) {
                    if (!isUserLoggedIn) {
                        callToActivity("LOGIN");
                    } else {
                        new AlertDialog.Builder(this)
                                .setTitle("Alert")
                                .setMessage(getResources().getString(R.string.are_you_sure))
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // continue with delete
                                        MySharedPreferences.getInstance().clearData(HomePage.this, "session_id");
                                        MySharedPreferences.getInstance().clearData(HomePage.this, "user_data");
                                        MySharedPreferences.getInstance().clearData(HomePage.this, "social_url");
                                        MySharedPreferences.getInstance().clearData(HomePage.this, "firebase_taken");
                                        isUserLoggedIn = false;
                                        deleteToken();

                                        makeArrayList();

                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();


                    }
                }

                break;

            case 2:
                position = 4;
                break;

            case 3:
                position = 3;
                break;

            case 4:
                callToActivity("ABOUT_US");
                break;

            case 5:
                callToActivity("CONTACT_US");
                break;

            case 6:
                sendLink();
                break;


            default:
                break;
        }


        changeFragment(position);


    }

    private void deleteToken()
    {
        if (Constant.haveNetworkConnection(HomePage.this))
        {
            Intent intent = new Intent(this,DeleteTokenService.class);
            this.startService(intent);
        }


    }

    private void sendLink() {

        Log.i("Send email", "");
        String[] TO = {""};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "hello");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "www.google.com");

        try {
            startActivityForResult(Intent.createChooser(emailIntent, "Send Via"), 110);

            Log.d("Finished sending.", "");
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(HomePage.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }


    }

    private void callToActivity(String key) {
        Intent fragment_activityy = new Intent(HomePage.this, FragmentActivity.class);
        fragment_activityy.putExtra("fragment", key);
        startActivity(fragment_activityy);

    }

    private void changeFragment(int position) {
        viewpager.setCurrentItem(position);
        mDrawerLayout.closeDrawers();

    }

    private void initViews() {
        viewpager = (NonSwipeableViewPager) findViewById(R.id.viewpager);
        titleTV = (TextView) findViewById(R.id.titleTV);

        this.realm = RealmController.with(this).getRealm();
        setupViewPager(viewpager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewpager);

        tabLayout.setSelectedTabIndicatorColor(Color.GRAY);
        tabLayout.setTabTextColors(Color.GRAY, Color.WHITE);

        setupTabIcons();
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
        tabLayout.getTabAt(4).setIcon(tabIcons[4]);
        Log.d("modeee","="+tabLayout.getTabMode());

    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new HomePageFragment(), getResources().getString(R.string.home));
        adapter.addFrag(new SearchFragment(), getResources().getString(R.string.search));
        adapter.addFrag(new WishlistFragment(), getResources().getString(R.string.wishlist));
        adapter.addFrag(new CartFragment(), getResources().getString(R.string.cart));
        adapter.addFrag(new MyAccount(), getResources().getString(R.string.my_account));
        viewPager.setAdapter(adapter);

        viewpager.setCurrentItem(0);
        viewPager.setOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                titleTV.setText(getResources().getString(R.string.home));
                break;
            case 1:
                titleTV.setText(getResources().getString(R.string.search));
                break;
            case 2:
                titleTV.setText(getResources().getString(R.string.wishlist));
                break;
            case 3:
                titleTV.setText(getResources().getString(R.string.my_cart));
                break;
            case 4:
                titleTV.setText(getResources().getString(R.string.my_account));
                break;


        }


    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constant.hideKeyboard(HomePage.this);
        mDrawerLayout.closeDrawers();
        int entry_count = getSupportFragmentManager().getBackStackEntryCount();

        //   Log.d("helper",""+helper+"count"+entry_count);
        if (entry_count == 0)
            finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
