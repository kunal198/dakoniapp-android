package com.dokani.Activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.dokani.MarshMallowPermissionClass.AbsRuntimeMarshmallowPermission;
import com.dokani.R;

/**
 * Created by brst-pc20 on 1/2/17.
 */

public class SplashScreen extends AbsRuntimeMarshmallowPermission {

    //TIME OF SPLASH
    private int SPLASH_TIME = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        requestAppPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},R.string.message,ALL_PERMISSIONS);


    }

    @Override
    public void onPermissionGranted(int requestCode) {
        if (requestCode == ALL_PERMISSIONS) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent home_page = new Intent(SplashScreen.this, HomePage.class);
                    startActivity(home_page);
                    finish();
                    overridePendingTransition(R.anim.animation_enter_from_right,R.anim.animation_leave_out_to_left);
                }
            }, SPLASH_TIME);
        }
    }
}