package com.dokani.Activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dokani.CommonUtils.Constant;
import com.dokani.Fragments.CashOnDeliveryFragment;
import com.dokani.Fragments.PayViaPaypalFragment;
import com.dokani.Fragments.PayWithCreditCard;
import com.dokani.MySharedPreferences;
import com.dokani.R;

import java.util.HashMap;
import java.util.Locale;


/**
 * Created by brst-pc20 on 1/5/17.
 */

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout creditCardRL, payViaPaypalRL, cashOnDeliveryRL;
    private TextView payViaCreditCardTV, payViaPaypalTV, cashOnDeliveryTV;
    private FrameLayout payViacashFL, payViaCardFL, payViaPaypalFL;
    public FrameLayout paymentFrame,checkoutFrame;

    private String ammount="";
    private HashMap hashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_checkout_activity);

         ammount =getIntent().getStringExtra("ammount");
        hashMap= (HashMap) getIntent().getSerializableExtra("all_data");
        initView();
        //  String fragment = getIntent().getStringExtra("fragment");

        if (savedInstanceState == null) {
            PayWithCreditCard payWithCreditCard = new PayWithCreditCard();
            payWithCreditCard.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.payment_frame_container, payWithCreditCard)
                    .commit();
        }


    }
    public void changeLanguage(View view)
    {
        String languageToLoad  = "ar";
        String data= MySharedPreferences.getInstance().getData(PaymentActivity.this,"current_lang");
        Log.d("dataaaa","=="+data);
        if (MySharedPreferences.getInstance().getData(PaymentActivity.this,"current_lang").equals("null") ||MySharedPreferences.getInstance().getData(PaymentActivity.this,"current_lang").equals("english"))
        {
            languageToLoad  = "ar";
            MySharedPreferences.getInstance().storeData(PaymentActivity.this,"current_lang","arabic");
        }
        else {


            languageToLoad  = "en_US";
            MySharedPreferences.getInstance().storeData(PaymentActivity.this,"current_lang","english");
        }


        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        restartActivity();
    }
    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        int entry_count=getSupportFragmentManager().getBackStackEntryCount();
        if (entry_count==0)
            finish();
    }

    private void initView() {


        paymentFrame = (FrameLayout) findViewById(R.id.paymentFrame);
        checkoutFrame = (FrameLayout) findViewById(R.id.checkoutFrame);
        payViaCreditCardTV = (TextView) findViewById(R.id.payViaCreditCardTV);

        payViaPaypalTV = (TextView) findViewById(R.id.payViaPaypalTV);
        cashOnDeliveryTV = (TextView) findViewById(R.id.cashOnDeliveryTV);

        creditCardRL = (RelativeLayout) findViewById(R.id.creditCardRL);

        payViaPaypalRL = (RelativeLayout) findViewById(R.id.payViaPaypalRL);
        cashOnDeliveryRL = (RelativeLayout) findViewById(R.id.cashOnDeliveryRL);

        payViacashFL = (FrameLayout) findViewById(R.id.payViacashFL);
        payViaCardFL = (FrameLayout) findViewById(R.id.payViaCardFL);
        payViaPaypalFL = (FrameLayout) findViewById(R.id.payViaPaypalFL);
        payViaCardFL.setOnClickListener(this);
        payViaPaypalFL.setOnClickListener(this);
        payViacashFL.setOnClickListener(this);
        payViaCardFL.performClick();
    }




    public void goBack(View view) {
        Constant.hideKeyboard(PaymentActivity.this);
        onBackPressed();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.payViaCardFL:
                paymentMode("CREDIT_CARD");
                break;

            case R.id.payViacashFL:
                paymentMode("CASH_ON_DELIVERY");
                break;
            case R.id.payViaPaypalFL:
                paymentMode("PAYPAL");
                break;
        }

    }

    private void paymentMode(String payment_way) {
        switch (payment_way) {
            case "CREDIT_CARD":
                setSelectedColors(payViaCreditCardTV, creditCardRL);
                setUnselectedColors(payViaPaypalTV, payViaPaypalRL);
                setUnselectedColors(cashOnDeliveryTV, cashOnDeliveryRL);

                PayWithCreditCard payWithCreditCard = new PayWithCreditCard();
                payWithCreditCard.setArguments(getIntent().getExtras());
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.payment_frame_container, payWithCreditCard)
                        .commit();


                break;
            case "PAYPAL":
                setSelectedColors(payViaPaypalTV, payViaPaypalRL);
                setUnselectedColors(cashOnDeliveryTV, cashOnDeliveryRL);
                setUnselectedColors(payViaCreditCardTV, creditCardRL);


                PayViaPaypalFragment payViaPaypalFragment = new PayViaPaypalFragment();
                payViaPaypalFragment.setArguments(getIntent().getExtras());
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.payment_frame_container, payViaPaypalFragment)

                        .commit();


                break;
            case "CASH_ON_DELIVERY":
                setSelectedColors(cashOnDeliveryTV, cashOnDeliveryRL);
                setUnselectedColors(payViaPaypalTV, payViaPaypalRL);
                setUnselectedColors(payViaCreditCardTV, creditCardRL);

                CashOnDeliveryFragment cashOnDeliveryFragment = new CashOnDeliveryFragment();
                cashOnDeliveryFragment.setArguments(getIntent().getExtras());
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.payment_frame_container, cashOnDeliveryFragment)
                        .commit();
                break;
        }

    }

    private void setSelectedColors(TextView textview, RelativeLayout relativeLayout) {

        textview.setBackgroundColor(Color.BLACK);
        textview.setTextColor(Color.WHITE);
        relativeLayout.setBackgroundColor(Color.BLACK);

    }

    private void setUnselectedColors(TextView textview, RelativeLayout relativeLayout) {

        textview.setTextColor(getResources().getColor(R.color.colorGrey));
        textview.setBackgroundColor(getResources().getColor(R.color.colorGreyVariant3));
        relativeLayout.setBackgroundColor(getResources().getColor(R.color.colorDarkGrey));

    }
}
