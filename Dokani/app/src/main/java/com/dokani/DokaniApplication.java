package com.dokani;
import android.app.Application;

import com.dokani.utils.ConnectivityReceiver;

import io.realm.Realm;
import io.realm.RealmConfiguration;
/**
 * Created by brst-pc20 on 1/25/17.
 */

public class DokaniApplication extends Application {

    public static DokaniApplication instance;

    @Override
    public void onCreate() {

        super.onCreate();
        instance=this;
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)

                .name("dokani_realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

    }

    public static synchronized DokaniApplication getInstance()
    {
        return instance;
    }

   /* public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }*/
}
