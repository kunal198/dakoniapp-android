package com.dokani.FireBaseService;

import android.util.Log;

import com.dokani.CommonUtils.MyNotification;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by brst-pc20 on 3/10/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        Log.d("log_tag", "From: " + remoteMessage.getFrom());
        Log.d("log_tag", "From: " + remoteMessage.getMessageId());

        if (remoteMessage.getData().size() > 0) {
            Log.d("log_tag", "Message data payload: " + remoteMessage.getData());
        }

        if (remoteMessage.getNotification() != null) {
            Log.d("log_tag", "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        MyNotification.showNotification(this,remoteMessage.getNotification().getBody(),1);

    }


        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.

    }

