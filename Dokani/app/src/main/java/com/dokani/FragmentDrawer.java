package com.dokani;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;




public class FragmentDrawer extends Fragment implements View.OnClickListener
{

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View                  containerView;
    ImageView windowCloseIV;
    TextView txtv_username;
    Context  context;

    public FragmentDrawer()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        context = getActivity();

        View layout = inflater.inflate(R.layout.fragment_fragment_drawer, container, false);

        windowCloseIV= (ImageView) layout.findViewById(R.id.windowCloseIV);
        windowCloseIV.setOnClickListener(this);

        return layout;
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar)
    {
        containerView = getActivity().findViewById(fragmentId);

        mDrawerLayout = drawerLayout;

        int width  = (getResources().getDisplayMetrics().widthPixels / 4);
        int final_width=(getResources().getDisplayMetrics().widthPixels -width);
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) containerView.getLayoutParams();
        params.width = final_width ;
        containerView.setLayoutParams(params);

        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close)
        {
            @Override
            public void onDrawerOpened(View drawerView)
            {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();

            }

            @Override
            public void onDrawerClosed(View drawerView)
            {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset)
            {
                super.onDrawerSlide(drawerView, slideOffset);

                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable()
        {
            @Override
            public void run()
            {
                mDrawerToggle.syncState();
            }
        });
    }

    @Override
    public void onClick(View view) {
        mDrawerLayout.closeDrawers();
    }
}