package com.dokani.RealmDataBase;


import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import com.dokani.Bean.DemoClass;

import io.realm.Realm;
import io.realm.RealmResults;


public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    //Refresh the realm istance
    public void refresh() {

        realm.refresh();
    }

    //clear all objects from DemoClass.class
    public void clearAll() {
        realm.beginTransaction();
        realm.clear(DemoClass.class);
        realm.commitTransaction();
    }

    //find all objects in the DemoClass.class
    public RealmResults<DemoClass> getDemoClasss() {

        return realm.where(DemoClass.class).findAll();
    }

    //query a single item with the given id
    public DemoClass getDemoClass(String id) {

        return realm.where(DemoClass.class).equalTo("id", id).findFirst();
    }

    //check if DemoClass.class is empty
    public boolean hasDemoClasss() {

        return !realm.allObjects(DemoClass.class).isEmpty();
    }

    //query example
    public RealmResults<DemoClass> queryedDemoClasss() {

        return realm.where(DemoClass.class)
                .contains("author", "Author 0")
                .or()
                .contains("title", "Realm")
                .findAll();

    }
}
