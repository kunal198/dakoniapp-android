package com.dokani.Adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.dokani.Fragments.FilterFragment;
import com.dokani.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by brst-pc20 on 1/27/17.
 */

public class FilterExpandableListAdapter extends BaseExpandableListAdapter {


    int price=-1,manufactor=-1,color=-1,category=-1;
    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;

    public FilterExpandableListAdapter(Context context, List<String> listDataHeader,
                                       HashMap<String, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        Log.d("data1---",""+_listDataHeader);
        Log.d("data2---",""+_listDataChild);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return this._listDataChild.get(this._listDataHeader.get(i))
                .size();
    }

    @Override
    public Object getGroup(int i) {
        return this._listDataHeader.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return this._listDataChild.get(this._listDataHeader.get(i)).get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {

        String headerTitle = (String) getGroup(i);
        if (view == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.expandable_group_view, null);
        }

        TextView lblListHeader = (TextView) view
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return view;
    }

    @Override
    public View getChildView(final int i, final int i1, boolean b, View view, ViewGroup viewGroup) {

        final String childText = (String) getChild(i, i1);

        if (view == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.filter_list_item, null);
        }

        final CheckBox childItemTV = (CheckBox) view.findViewById(R.id.childValueCB);
        String[] value=childText.split("@");
        childItemTV.setText(value[0].trim());

        Log.e("HHHHH",""+i1+"----I----"+i+"-----"+price);

        if(getGroup(i).equals("Price")) {
            if (price == i1) {
                childItemTV.setChecked(true);
            } else {
                childItemTV.setChecked(false);
            }
        }

       if (getGroup(i).equals("Manufacture"))
       {
           if (manufactor==i1 )
           {
               childItemTV.setChecked(true);
           }
           else{
               childItemTV.setChecked(false);
           }
       }
        if (getGroup(i).equals("Color"))
        {
            if (color==i1 )
            {
                childItemTV.setChecked(true);
            }
            else{
                childItemTV.setChecked(false);
            }
        } if (getGroup(i).equals("category"))
        {
            if (category==i1 )
            {
                childItemTV.setChecked(true);
            }
            else{
                childItemTV.setChecked(false);
            }
        }



        childItemTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox cb= (CheckBox) view;
                    Log.d("clickkkkkkk",""+getChild(i,i1));
                    Log.d("clickkkkkkk",""+i1+"=="+i);
                if (getGroup(i).equals("category"))
                {  Log.d("clickkkkkkk","--"+childItemTV.isChecked());
                    if (!childItemTV.isChecked())
                    {
                        FilterFragment.category_value="";
                        category=-1;
                    }
                    else{
                        String[] value=_listDataChild.get(_listDataHeader.get(i)).get(i1).split("@");
                        FilterFragment.category_value=value[1];
                        category=i1;
                    }

                } if (getGroup(i).equals("Price"))
                {  Log.d("clickkkkkkk","--"+childItemTV.isChecked());
                    if (!childItemTV.isChecked())
                    {
                        FilterFragment.price_value="";
                        price=-1;
                    }
                    else{
                        String[] value=_listDataChild.get(_listDataHeader.get(i)).get(i1).split("@");
                        FilterFragment.price_value=value[1];
                        price=i1;
                    }

                }
                else if (getGroup(i).equals("Manufacture"))
                {
                    if (!childItemTV.isChecked())
                    {

                        FilterFragment.manufactor_value="";
                        manufactor=-1;
                    }
                    else{
                        String[] value=_listDataChild.get(_listDataHeader.get(i)).get(i1).split("@");
                        FilterFragment.manufactor_value=value[1];
                        manufactor=i1;
                    }

                }
                else if (getGroup(i).equals("Color"))
                {
                    if (!childItemTV.isChecked())
                    {
                        FilterFragment.color_value="";
                        color=-1;
                    }
                    else{
                        String[] value=_listDataChild.get(_listDataHeader.get(i)).get(i1).split("@");
                        FilterFragment.color_value=value[1];
                        color=i1;
                    }

                }


               notifyDataSetChanged();
            }
        });
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
