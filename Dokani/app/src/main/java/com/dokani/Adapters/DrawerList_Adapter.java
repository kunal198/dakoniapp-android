package com.dokani.Adapters;



import android.content.Context;
import android.media.Image;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.ImageView;


import com.dokani.CommonUtils.Constant;
import com.dokani.R;

import java.util.ArrayList;

/**
 * Created by sharan on 24/11/15.
 */
public class DrawerList_Adapter extends BaseAdapter {

    Context con;
    ArrayList<String> list;
    ArrayList<Integer> image_list;
    ArrayList<Integer> image_list_selected;
    int selectedposition;

    public DrawerList_Adapter(Context con, ArrayList<String> list,ArrayList<Integer> image_list,ArrayList<Integer> image_list_selected, int selectedposition) {
        this.con = con;
        this.list = list;
        this.image_list = image_list;

        this.selectedposition = selectedposition;
        this.image_list_selected = image_list_selected;

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View row, ViewGroup parent) {


                    LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    row = inflater.inflate(R.layout.custom_drawerlist_item, parent, false);

                    TextView txtv_drawer_item = (TextView) row.findViewById(R.id.txtv_drawer_item);
                    ImageView drawerLogoIV = (ImageView) row.findViewById(R.id.drawerLogoIV);
                    drawerLogoIV.setImageResource(image_list.get(position));
                    txtv_drawer_item.setText(list.get(position));

                    if (selectedposition == position) {
                        txtv_drawer_item.setTextColor(ContextCompat.getColor(con, R.color.black));
                        row.setBackgroundColor(ContextCompat.getColor(con, R.color.colorGreyVariant3));
                        drawerLogoIV.setImageResource(image_list_selected.get(position));
                    }


        return row;
    }

    public void changeSelectedBackground(int psition) {
        selectedposition = psition;
    }

}