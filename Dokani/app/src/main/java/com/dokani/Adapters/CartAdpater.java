package com.dokani.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dokani.Bean.DemoClass;
import com.dokani.CommonUtils.Constant;
import com.dokani.Fragments.CartFragment;
import com.dokani.PopUp.RatingPopUp;
import com.dokani.R;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by brst-pc20 on 1/9/17.
 */

public class CartAdpater extends RecyclerView.Adapter<CartAdpater.MyViewHolder> {

    LinearLayout ratingBarLL;
    Context context;
    RealmResults<DemoClass> results;
    Realm realm;
    Fragment fragment;
    RecyclerView recyclerView;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView quantityTV, prodName, priceTV, removeItemTV;
        private RatingBar ratingBarRB;

        private ImageView productIV, addIV, subtractIV;

        public MyViewHolder(View view) {
            super(view);

            productIV = (ImageView) view.findViewById(R.id.productIV);
            addIV = (ImageView) view.findViewById(R.id.addIV);
            subtractIV = (ImageView) view.findViewById(R.id.subtractIV);
            quantityTV = (TextView) view.findViewById(R.id.quantityTV);
            prodName = (TextView) view.findViewById(R.id.prodName);
            priceTV = (TextView) view.findViewById(R.id.priceTV);
            removeItemTV = (TextView) view.findViewById(R.id.removeItemTV);
            ratingBarRB = (RatingBar) view.findViewById(R.id.ratingBarRB);
            ratingBarLL = (LinearLayout) view.findViewById(R.id.ratingBarLL);
            ratingBarLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String url[] = results.get(getAdapterPosition()).getCartProductUrl().toString().split("@");
                    RatingPopUp ratingPopUp = new RatingPopUp(context, results.get(getAdapterPosition()).getCartProductId().toString(), url[0]);
                    ratingPopUp.show();

                    ratingPopUp.getWindow().setLayout(context.getResources().getDisplayMetrics().widthPixels * 90 / 100,
                            ViewGroup.LayoutParams.WRAP_CONTENT);

                    ratingPopUp.setCancelable(false);
                }
            });

            subtractIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = Integer.parseInt(quantityTV.getText().toString());
                    if (quantity >= 2) {


                        Log.d("index", "==" + getAdapterPosition());
                        updateProductQuantity(getAdapterPosition(), "sub");
                        quantity--;

                        quantityTV.setText(String.valueOf(quantity));
                    }


                }
            });
            addIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        String stock_availability[] = results.get(getAdapterPosition()).getCartProductAvailabilityQty().toString().split(":");
                        int stock_available = Integer.parseInt(stock_availability[0]);

                        int quantity = Integer.parseInt(quantityTV.getText().toString());

                        if (quantity<stock_available) {
                            quantity++;
                            updateProductQuantity(getAdapterPosition(), "add");
                            quantityTV.setText(String.valueOf(quantity));
                        } else {
                            Constant.showAlert2(context,context.getResources().getString(R.string.alert),context.getResources().getString(R.string.ur_order_no)+stock_available+context.getResources().getString(R.string.placed_success));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            removeItemTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    realm.beginTransaction();
                    results.remove(getAdapterPosition());
                    realm.commitTransaction();
                    notifyDataSetChanged();
//                    CartFragment.totalProductsTV.setText(results.size()+"");
                    if (results.size() == 0) {
                        ((CartFragment) fragment).noDataLayoutRL.setVisibility(View.VISIBLE);
                        ((CartFragment) fragment).dataLayoutLL.setVisibility(View.GONE);
                    } else {
                        ((CartFragment) fragment).noDataLayoutRL.setVisibility(View.GONE);
                        ((CartFragment) fragment).dataLayoutLL.setVisibility(View.VISIBLE);
                    }

                }
            });
        }
    }


    public CartAdpater(Context context, RealmResults<DemoClass> results, Realm realm, Fragment fragment, RecyclerView recyclerView) {

        this.context = context;
        this.results = results;
        this.realm = realm;
        this.fragment = fragment;
        this.recyclerView = recyclerView;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_cart_row_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        //  Cartbean cartbean = quantity_list.get(position);
        //  holder.quantityTV.setText(""+cartbean.getGetQuantity());

        String prod_name[] = results.get(position).getCartProductName().split(":");
        String prod_price[] = results.get(position).getCartProductPrice().split(":");
        String price = String.format("%.02f", Float.parseFloat(prod_price[0]));
        String prod_url[] = results.get(position).getCartProductUrl().split("@");
        String prod_id[] = results.get(position).getCartProductId().split(":");
        String prod_rating[] = results.get(position).getCartProductRating().split(":");
        String prod_qty[] = results.get(position).getCartProductQuantity().split(":");
        holder.ratingBarRB.setRating(Float.parseFloat(prod_rating[0]));
        holder.prodName.setText(prod_name[0]);
        holder.priceTV.setText("OMR"+price);
        holder.quantityTV.setText(prod_qty[0]);
        Log.d("prod_url", "" + prod_url[0]);
        Glide.with(context).load(prod_url[0]).into(holder.productIV);


    }

    @Override
    public int getItemCount() {
        return results.size();
    }


    private void updateProductQuantity(int index, String operation) {
        RealmResults<DemoClass> result = realm.where(DemoClass.class).
                contains("cartProductId", results.get(index).getCartProductId()).findAll();
        Log.d("result>>", "==" + result);

        realm.beginTransaction();
        String qty[] = result.get(0).getCartProductQuantity().split(":");
        if (operation.equals("add")) {

            int qtyy = Integer.parseInt(qty[0]) + 1;
            result.get(0).setCartProductQuantity(String.valueOf(qtyy) + ":qty");
        } else {
            int qtyy = Integer.parseInt(qty[0]) - 1;
            result.get(0).setCartProductQuantity(String.valueOf(qtyy) + ":qty");
        }
        realm.commitTransaction();
    }

}