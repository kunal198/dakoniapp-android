package com.dokani.Adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dokani.Bean.Country;
import com.dokani.R;

import java.util.ArrayList;

public class SortSpinnerAdapter1 extends ArrayAdapter<Country> {

    private Context con;
    ArrayList<Country> countryArrayList;
        int ilayout;



    public SortSpinnerAdapter1(Context context, int resource, ArrayList<Country> countryArrayList) {
        super(context, resource);
        this.con=context;
        this.countryArrayList=countryArrayList;

       // mKeys = sortingItem.keySet().toArray(new String[sortingItem.size()]);
        this.ilayout=resource;
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(ilayout, null, true);

        TextView sortItemTV= (TextView) convertView.findViewById(R.id.sortItemmTV);
        sortItemTV.setText(countryArrayList.get(position).getCountryName().toString());
        return convertView;
    }


    @Override
    public int getCount() {
        return countryArrayList.size();
    }

    @Nullable
    @Override
    public Country getItem(int position) {
        return countryArrayList.get(position);
    }
}
