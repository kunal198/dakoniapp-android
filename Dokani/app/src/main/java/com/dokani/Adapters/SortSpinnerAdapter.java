package com.dokani.Adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dokani.R;

import java.util.ArrayList;

public class SortSpinnerAdapter extends ArrayAdapter<String> {

    private Context con;
    ArrayList<String> sortingItem;
        int ilayout;



    ArrayList<String> results;

    public SortSpinnerAdapter(Context context, int resource,ArrayList<String> sortingItem) {
        super(context, resource);
        this.con=context;
        this.sortingItem=sortingItem;
        this.ilayout=resource;
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(ilayout, null, true);

        TextView sortItemTV= (TextView) convertView.findViewById(R.id.sortItemmTV);
        sortItemTV.setText(sortingItem.get(position));
        return convertView;
    }


    @Override
    public int getCount() {
        return sortingItem.size();
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return sortingItem.get(position);
    }
}
