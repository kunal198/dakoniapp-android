package com.dokani.Adapters;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dokani.Activity.FragmentActivity;
import com.dokani.Bean.DemoClass;
import com.dokani.Bean.HomePageBean;
import com.dokani.CommonUtils.Constant;
import com.dokani.MySharedPreferences;
import com.dokani.PopUp.RatingPopUp;
import com.dokani.R;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class SearchListAdapter extends BaseAdapter  {

    private String TAG = getClass().getSimpleName();
    private Context con;
    Realm realm;
    ViewHolder mViewHolder;
    private ArrayList<HomePageBean> imageIdList = new ArrayList<>();


    public SearchListAdapter(Context con, ArrayList<HomePageBean> imageIdList,Realm realm) {
        Log.d(TAG, "Constructor" + imageIdList.size());
        this.realm=realm;
        this.con = con;
        this.imageIdList = imageIdList;


    }


    @Override
    public int getCount() {
        return imageIdList.size();
    }

    @Override
    public Object getItem(int i) {
        return imageIdList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (view == null) {
            mViewHolder = new ViewHolder();
            view = inflater.inflate(R.layout.search_result, null, true);
            mViewHolder.productIV = (ImageView) view.findViewById(R.id.productIV);
            mViewHolder.priceTV = (TextView) view.findViewById(R.id.priceTV);
            mViewHolder.productNameTV = (TextView) view.findViewById(R.id.productNameTV);
            mViewHolder.smallDescriptionTV = (TextView) view.findViewById(R.id.smallDescriptionTV);
            mViewHolder.heartIV = (ImageView) view.findViewById(R.id.heartIV);
            mViewHolder.searchCheckoutIV = (ImageView) view.findViewById(R.id.searchCheckoutIV);
            mViewHolder.searchCartIV = (ImageView) view.findViewById(R.id.searchCartIV);
            mViewHolder.ratingBarSearchRB = (RatingBar) view.findViewById(R.id.ratingBarSearchRB);
            mViewHolder.ratingBarLL = (LinearLayout) view.findViewById(R.id.ratingBarLL);



            view.setTag(mViewHolder);

        } else {
            mViewHolder = (ViewHolder) view.getTag();
        }
        Log.d("loggggggg","entered"+imageIdList.size());
///        Log.d("productUrl", "" + imageIdList.get(i).getProductUrl());
       Glide.with(con).load(imageIdList.get(i).getProductUrl()).into(mViewHolder.productIV);
        if (!imageIdList.get(i).getProductPrice().equals("null") && !imageIdList.get(i).getProductPrice().equals(""))
        {

            String price = String.format("%.02f", Float.parseFloat(imageIdList.get(i).getProductPrice()));
            mViewHolder.priceTV.setText("OMR" + price);
        }


        mViewHolder.smallDescriptionTV.setText(imageIdList.get(i).getProductSmallDesc());
        mViewHolder.productNameTV.setText(imageIdList.get(i).getProductName());
        mViewHolder.ratingBarSearchRB.setRating(Float.parseFloat(imageIdList.get(i).getProductRating()));
        mViewHolder.searchCheckoutIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String session = MySharedPreferences.getInstance().getData(con, "session_id");
                if (session.equals("null")) {
                    askForLogin(i);

                }
                else {
                    Intent fragment_activityy = new Intent(con, FragmentActivity.class);
                    fragment_activityy.putExtra("fragment", "CHECK_OUT_SEARCH");
                    fragment_activityy.putExtra("product_id",imageIdList.get(i).getProductId());
                    fragment_activityy.putExtra("qty","1");
                    fragment_activityy.putExtra("qty_size","1");
                    fragment_activityy.putExtra("subtotal",imageIdList.get(i).getProductPrice());
                    con.startActivity(fragment_activityy);
                }


            }
        });
        mViewHolder.searchCartIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("position",""+i);
                RealmResults<DemoClass> results = realm.where(DemoClass.class).
                        contains("cartProductId",imageIdList.get(i).getProductId()+":id").findAll();
                if (results.size()<1) {
                    DemoClass demoClass = new DemoClass();
                    demoClass.setId(3 + System.currentTimeMillis());
                    demoClass.setCartProductName(imageIdList.get(i).getProductName() + ":name");
                    demoClass.setCartProductPrice(imageIdList.get(i).getProductPrice() + ":price");
                    demoClass.setCartProductUrl(imageIdList.get(i).getProductUrl() + "@url");
                    demoClass.setCartProductId(imageIdList.get(i).getProductId() + ":id");
                    demoClass.setCartProductAvailabilityQty(imageIdList.get(i).getProductAvailableQty() + ":qty");
                    demoClass.setCartProductQuantity("1"+":qty");
                    demoClass.setCartProductRating(imageIdList.get(i).getProductRating() + ":rating");
                    Constant.copyDataToRealm(realm, demoClass);
                    Constant.initiatePopupWindow(con.getResources().getString(R.string.product_added_to_cart), con);
                }
                else{
                    realm.beginTransaction();
                    String qty[]=results.get(0).getCartProductQuantity().split(":");
                    int quantity= Integer.parseInt(qty[0])+1;
                    results.get(0).setCartProductQuantity(String.valueOf(quantity));
                    realm.commitTransaction();
                    Constant.initiatePopupWindow(con.getResources().getString(R.string.product_qty_updated), con);
                    Log.d("getData","-id  present--");
                }
            }
        });
        mViewHolder.heartIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RealmResults<DemoClass> results = realm.where(DemoClass.class).
                        contains("wishProductId",imageIdList.get(i).getProductId()+":id").findAll();

                if (results.size()<1)
                {
                    DemoClass demoClass = new DemoClass();
                    demoClass.setId(4+System.currentTimeMillis());
                    demoClass.setWishProductName(imageIdList.get(i).getProductName()+":name");
                    demoClass.setWishProductPrice(imageIdList.get(i).getProductPrice()+":price");
                    demoClass.setWishProductUrl(imageIdList.get(i).getProductUrl()+"@url");
                  // demoClass.setWishProductUrl(imageIdList.get(i).getProductAvailableQty()+"@url");
                    demoClass.setWishProductId(imageIdList.get(i).getProductId()+":id");
                    demoClass.setWishProductRating(imageIdList.get(i).getProductRating()+":rating");
                    demoClass.setWishProductDesc(imageIdList.get(i).getProductSmallDesc()+"@description");
                    Constant.copyDataToRealm(realm, demoClass);
                    Constant.initiatePopupWindow(con.getResources().getString(R.string.product_added_in_wish), con);

                }
                else{
                    Constant.initiatePopupWindow(con.getResources().getString(R.string.product_already_in_wish), con);
                    Log.d("getData","-id  present--");
                }

            }
        });
        mViewHolder.ratingBarLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                RatingPopUp ratingPopUp = new RatingPopUp(con,imageIdList.get(i).getProductId().toString(),imageIdList.get(i).getProductUrl().toString());
                ratingPopUp.show();

                ratingPopUp.getWindow().setLayout(con.getResources().getDisplayMetrics().widthPixels * 90 / 100,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                ratingPopUp.setCancelable(false);
            }
/*
            @Override
            public boolean onClick(View view, MotionEvent motionEvent) {

                return false;
            }*/
        });
        return view;
    }



    static class ViewHolder {
        ImageView productIV, searchCheckoutIV, searchCartIV, heartIV;
        RatingBar ratingBarSearchRB;
        TextView priceTV, productNameTV,smallDescriptionTV;
        LinearLayout ratingBarLL;
    }
    private void askForLogin(final int i) {

        final String[] stringItems = {con.getResources().getString(R.string.login),con.getResources().getString(R.string.checkout_guest)};

        final ActionSheetDialog dialog = new ActionSheetDialog(con, stringItems, null,con.getResources().getString(R.string.cancel));
        dialog.isTitleShow(false).show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {


                if (position == 0) {
                    Log.e("pos1", "dalsjhdjk");
                    Intent login = new Intent(con, FragmentActivity.class);
                    login.putExtra("fragment", "LOGIN");
                   con.startActivity(login);

                } else if (position == 1) {
                    Log.e("pos2", "dalsjhdjk");
                    Log.d("getProductId()","--"+imageIdList.get(i).getProductId());
                    Intent fragment_activityy = new Intent(con, FragmentActivity.class);
                    fragment_activityy.putExtra("fragment", "CHECK_OUT_SEARCH");
                    fragment_activityy.putExtra("product_id",imageIdList.get(i).getProductId());
                    fragment_activityy.putExtra("qty","1");
                    fragment_activityy.putExtra("qty_size","1");
                    fragment_activityy.putExtra("subtotal",imageIdList.get(i).getProductPrice());
                    con.startActivity(fragment_activityy);


                }
                dialog.dismiss();

            }
        });
    }

}
