package com.dokani.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dokani.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by brst-pc20 on 1/9/17.
 */

public class ProductAdapter extends BaseAdapter {

    Context context;
    ArrayList products_arraylist;
    ViewHolder mViewHolder;




    public ProductAdapter(Context context, HashMap<String, String> products_list) {

        this.context = context;
        products_arraylist = new ArrayList<>();
    Log.d("products_list","--"+products_list.entrySet());
        products_arraylist.addAll(products_list.entrySet());
    }



    @Override
    public int getCount() {
        return products_arraylist.size();
    }

    @Override
    public Map.Entry<String, String> getItem(int position) {
        return (Map.Entry) products_arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO implement you own logic with ID
        return position;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            mViewHolder = new ViewHolder();
            view = inflater.inflate(R.layout.expandable_list_item, null, true);

            mViewHolder.childItemTV = (TextView) view.findViewById(R.id.childItemTV);




            view.setTag(mViewHolder);

        } else {
            mViewHolder = (ViewHolder) view.getTag();
        }
        Map.Entry<String, String> item = getItem(i);
        Log.d("item.getKey()","="+item.getKey());
        // TODO replace findViewById by ViewHolder
        mViewHolder.childItemTV.setText(item.getKey());

     /*   mViewHolder.childItemTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/

        return view;


    }
    static class ViewHolder {

        private TextView childItemTV;
    }

    public  void addList(HashMap<String, String> products_list)
    {
        products_arraylist.addAll(products_list.entrySet());
    }

}