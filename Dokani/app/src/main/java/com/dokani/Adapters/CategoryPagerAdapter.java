package com.dokani.Adapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.RelativeLayout;

import com.dokani.R;

import java.util.ArrayList;

public class CategoryPagerAdapter extends  BaseAdapter{

    private String TAG=getClass().getSimpleName();
    private Context con;
    ViewHolder mViewHolder;
    private ArrayList<Integer> imageIdList = new ArrayList<>();
    private ArrayList<String> nameList = new ArrayList<>();

    public CategoryPagerAdapter(Context con, ArrayList<Integer> imageIdList ,ArrayList<String> nameList) {
        Log.d(TAG,"Constructor");
        this.con = con;

        this.imageIdList = imageIdList;
        this.nameList=nameList;


    }


    @Override
    public int getCount() {
        return nameList.size();
    }

    @Override
    public Object getItem(int i) {
        return nameList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater= (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (view==null)
        {
            mViewHolder=new ViewHolder();
            view=inflater.inflate(R.layout.category_custom_view,null,true);
            mViewHolder.categoryTV= (TextView) view.findViewById(R.id.categoryTV);
            view.setTag(mViewHolder);

        }
        else{
            mViewHolder = (ViewHolder) view.getTag();
        }
        mViewHolder.categoryTV.setCompoundDrawablesWithIntrinsicBounds(0,imageIdList.get(0),0,0);
        mViewHolder.categoryTV.setText(nameList.get(i));

        return view;
    }
    static class ViewHolder {
        TextView categoryTV;
    }
}
