package com.dokani.Adapters;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dokani.Activity.FragmentActivity;
import com.dokani.Bean.DemoClass;
import com.dokani.CommonUtils.Constant;
import com.dokani.Fragments.WishlistFragment;
import com.dokani.MySharedPreferences;
import com.dokani.PopUp.RatingPopUp;
import com.dokani.R;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;

public class WishListAdapter extends BaseAdapter implements View.OnTouchListener{

    private String TAG = getClass().getSimpleName();
    private Context con;
    ViewHolder mViewHolder;
    ArrayList<DemoClass> results;
    Realm realm;
    WishlistFragment fragment;


    public WishListAdapter(Context con, ArrayList<DemoClass> results, Realm realm, WishlistFragment fragment) {
        Log.d(TAG, "Constructor");
        this.con = con;
        this.results = results;
        this.realm = realm;
        this.fragment = fragment;
    }


    @Override
    public int getCount() {
        return results.size();
    }

    @Override
    public Object getItem(int i) {
        return results.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (view == null) {
            mViewHolder = new ViewHolder();
            view = inflater.inflate(R.layout.wishlist, null, true);
            mViewHolder.productIV = (ImageView) view.findViewById(R.id.productIV);
            mViewHolder.wishListCartIV = (ImageView) view.findViewById(R.id.wishListCartIV);
            mViewHolder.wishListCheckOutIV = (ImageView) view.findViewById(R.id.wishListCheckOutIV);
            mViewHolder.ratingBarRB = (RatingBar) view.findViewById(R.id.ratingBarRB);
            mViewHolder.priceTV = (TextView) view.findViewById(R.id.priceTV);
            mViewHolder.productNameTV = (TextView) view.findViewById(R.id.productNameTV);
            mViewHolder.smallDescriptionTV = (TextView) view.findViewById(R.id.smallDescriptionTV);
            mViewHolder.ratingBarLL = (LinearLayout) view.findViewById(R.id.ratingBarLL);
            mViewHolder.ratingBarLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try{
                    String url[]=results.get(i).getWishProductUrl().toString().split("@");
                    RatingPopUp ratingPopUp = new RatingPopUp(con,results.get(i).getWishProductId().toString(),url[0]);
                    ratingPopUp.show();

                    ratingPopUp.getWindow().setLayout(con.getResources().getDisplayMetrics().widthPixels * 90 / 100,
                            ViewGroup.LayoutParams.WRAP_CONTENT);

                    ratingPopUp.setCancelable(false);
                }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            });

            mViewHolder.ratingBarRB.setOnTouchListener(this);

            view.setTag(mViewHolder);

        } else {
            mViewHolder = (ViewHolder) view.getTag();
        }
        try {
            String wish_prod_name[] = results.get(i).getWishProductName().split(":");
            final String wish_prod_price[] = results.get(i).getWishProductPrice().split(":");
            String wish_prod_url[] = results.get(i).getWishProductUrl().split("@");
            String wish_prod_desc[] = results.get(i).getWishProductDesc().split("@");
            final String wish_prod_id[] = results.get(i).getWishProductId().split(":");
            String wish_prod_rating[] = results.get(i).getWishProductRating().split(":");
            String price1 = String.format("%.02f", Float.parseFloat(wish_prod_price[0]));

            Glide.with(con).load(wish_prod_url[0]).into(mViewHolder.productIV);
            mViewHolder.productNameTV.setText(wish_prod_name[0]);
            mViewHolder.priceTV.setText("OMR"+price1);
            mViewHolder.ratingBarRB.setRating(Float.parseFloat(wish_prod_rating[0]));
            mViewHolder.smallDescriptionTV.setText(wish_prod_desc[0]);
            mViewHolder.wishListCheckOutIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String session = MySharedPreferences.getInstance().getData(con, "session_id");
                    if (session.equals("null")) {
                        askForLogin(wish_prod_id[0],wish_prod_price[0]);
                    } else {
                        doTransactionOnCheckout(wish_prod_id[0],wish_prod_price[0]);
                    }

                }
            });
            mViewHolder.wishListCartIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    RealmResults<DemoClass> resultss = realm.where(DemoClass.class).
                            contains("cartProductId",results.get(i).getWishProductId()+":id").findAll();
                    Log.d("resultss","---"+resultss.toString());
                    if (resultss.size()<1) {
                        DemoClass demoClass = new DemoClass();
                        demoClass.setId(3 + System.currentTimeMillis());
                        demoClass.setCartProductName(results.get(i).getWishProductName() + ":name");
                        demoClass.setCartProductPrice(results.get(i).getWishProductPrice() + ":price");
                        demoClass.setCartProductUrl(results.get(i).getWishProductUrl() + "@url");
                        demoClass.setCartProductId(results.get(i).getWishProductId() + ":id");
                        demoClass.setCartProductQuantity("1"+":qty");
                        demoClass.setCartProductRating(results.get(i).getWishProductRating() + ":rating");
                        Constant.copyDataToRealm(realm, demoClass);
                        Constant.initiatePopupWindow(con.getResources().getString(R.string.product_added_to_cart), con);

                        realm.beginTransaction();
                        results.get(i).removeFromRealm();
                        results.remove(i);
                        realm.commitTransaction();
                        notifyDataSetChanged();
                        if (results.size()==0)
                        {


                            (fragment).wishnoDataLayoutRL.setVisibility(View.VISIBLE);
                            (fragment).wishListDataLayoutLL.setVisibility(View.GONE);

                        }

                        //realm.beginTransaction();



                    }
                    else{
                        realm.beginTransaction();
                        results.get(i).removeFromRealm();
                        results.remove(i);
                        realm.commitTransaction();
                        notifyDataSetChanged();
                        Constant.initiatePopupWindow(con.getResources().getString(R.string.product_already_in_cart), con);
                        Log.d("getData","-id  present--");
                    }
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return view;


    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        Log.d("hello", "popup");
       /* RatingPopUp ratingPopUp = new RatingPopUp(con);
        ratingPopUp.show();

        ratingPopUp.getWindow().setLayout(con.getResources().getDisplayMetrics().widthPixels * 90 / 100,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        ratingPopUp.setCancelable(false);*/
        return false;
    }

    static class ViewHolder {
         private ImageView productIV, wishListCheckOutIV, wishListCartIV;
        private RatingBar ratingBarRB;
        private LinearLayout ratingBarLL;
        private TextView priceTV,productNameTV,smallDescriptionTV;
    }
    public void clearList()
    {
//        this.results.clear();
      results.clear();
    }
    public void addList(ArrayList<DemoClass> results)
    {
        this.results.clear();
        this.results.addAll(results);
    }
    private void askForLogin(final String idd, final String price) {

        final String[] stringItems = {con.getResources().getString(R.string.login),con.getResources().getString(R.string.checkout_guest)};

        final ActionSheetDialog dialog = new ActionSheetDialog(con, stringItems, null,con.getResources().getString(R.string.cancel));
        dialog.isTitleShow(false).show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {


                if (position == 0) {
                    Log.e("pos1", "dalsjhdjk");
                    Intent login = new Intent(con, FragmentActivity.class);
                    login.putExtra("fragment", "LOGIN");
                    con.startActivity(login);

                } else if (position == 1) {
                    Log.e("pos2", "dalsjhdjk");

                    doTransactionOnCheckout(idd,price);

                }
                dialog.dismiss();

            }
        });
    }

    private void doTransactionOnCheckout(String id,String price) {
        Intent fragment_activityy = new Intent(con, FragmentActivity.class);
        fragment_activityy.putExtra("fragment", "CHECK_OUT_SEARCH");
        fragment_activityy.putExtra("product_id",id);
        fragment_activityy.putExtra("qty","1");
        fragment_activityy.putExtra("qty_size","1");
        fragment_activityy.putExtra("subtotal",price);
        con.startActivity(fragment_activityy);
    }
}
