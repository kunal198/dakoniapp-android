package com.dokani.Adapters;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.dokani.Bean.HomePageBean;
import com.dokani.R;

import java.util.ArrayList;

public class NewArrivalAdapter extends  BaseAdapter{

    private String TAG=getClass().getSimpleName();
    private Context con;

    ViewHolder mViewHolder;
    private ArrayList<HomePageBean> imageIdList ;


    public NewArrivalAdapter(Context con, ArrayList<HomePageBean> imageIdList ) {
        Log.d(TAG,"Constructor");
        this.con = con;

        this.imageIdList = imageIdList;



    }


    @Override
    public int getCount() {
        return imageIdList.size();
    }

    @Override
    public Object getItem(int i) {
        return imageIdList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater= (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (view==null)
        {
            mViewHolder=new ViewHolder();
            view=inflater.inflate(R.layout.new_arrival_custom_view,null,true);
            mViewHolder.newArrivalIV= (ImageView) view.findViewById(R.id.newArrivalIV);
            view.setTag(mViewHolder);

        }
        else{
            mViewHolder = (ViewHolder) view.getTag();
        }
        Glide.with(con).load(imageIdList.get(i).getProductUrl()).placeholder(R.mipmap.ic_banner_placeholder).into(mViewHolder.newArrivalIV);

        return view;
    }
    static class ViewHolder {
        ImageView newArrivalIV;
    }
}
