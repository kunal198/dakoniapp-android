package com.dokani.Adapters;


import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.dokani.Bean.HomePageBean;
import com.dokani.R;

import java.util.ArrayList;

public class ImagePagerAdapter extends PagerAdapter {

    private String TAG = getClass().getSimpleName();
    private Context con;
    private HomePageBean homePageBean;
    int layout;
    private ArrayList<String> imageIdList = new ArrayList<>();

    public ImagePagerAdapter(Context con, ArrayList<String> banner_list,int layout) {
        Log.d(TAG, "Constructor");
        this.con = con;
        imageIdList=banner_list;
        this.layout=layout;

    }

    @Override
    public int getCount() {
        return imageIdList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup row, int position) {
        LayoutInflater inflater = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(layout, null);

        ImageView image = (ImageView) v.findViewById(R.id.banner_image);
        Log.d("data------",""+imageIdList.get(position));
        Glide.with(con).load(imageIdList.get(position)).placeholder(R.mipmap.ic_banner_placeholder).into(image);
        row.addView(v);

        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }


}
